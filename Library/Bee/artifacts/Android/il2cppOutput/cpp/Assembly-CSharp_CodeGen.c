﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void SoundEffectsPlayer::ballHit()
extern void SoundEffectsPlayer_ballHit_mB7175961660F44CA08B0D59C46A998DDD9BC8AE3 (void);
// 0x00000002 System.Void SoundEffectsPlayer::pointGainedSound()
extern void SoundEffectsPlayer_pointGainedSound_mB4F2893405AA1D5EEA89A0EA64853AA3F958DCB4 (void);
// 0x00000003 System.Void SoundEffectsPlayer::.ctor()
extern void SoundEffectsPlayer__ctor_m7CC4A5526F138EED802681BB3E482E03EDA5A0D7 (void);
// 0x00000004 System.Void BallDisplay::Start()
extern void BallDisplay_Start_m3253F4F280159847A3F6E18C7F806F2DB78FD94E (void);
// 0x00000005 System.Void BallDisplay::.ctor()
extern void BallDisplay__ctor_mD74AF876C1888FD81F9451E10EB7C4F3F0CC3BB6 (void);
// 0x00000006 System.Void CollisionControl::Start()
extern void CollisionControl_Start_m94E3FF19E61F65014148879191D5C21EE6C50AB8 (void);
// 0x00000007 System.Void CollisionControl::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void CollisionControl_OnCollisionEnter2D_m4DD0B090C017B2C7B21100AAB2EEEA6F65E7C2E5 (void);
// 0x00000008 System.Void CollisionControl::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CollisionControl_OnTriggerEnter2D_m5369B8FE04ECB8EE602150EEDB740349BF03625E (void);
// 0x00000009 System.Void CollisionControl::.ctor()
extern void CollisionControl__ctor_mF95690D06C9CA4A70B46CCEE2BCD1D0209404E8F (void);
// 0x0000000A System.Void Movement::Start()
extern void Movement_Start_m3D09153FD03F06C31BBD151C21BA361EA49FA72B (void);
// 0x0000000B System.Collections.IEnumerator Movement::StartBall()
extern void Movement_StartBall_m8653974505239385E5EC1A085953760A71BA06CE (void);
// 0x0000000C System.Collections.IEnumerator Movement::StartBallBegin()
extern void Movement_StartBallBegin_mB4BFD1CCD96D9512D37B1560658928D5E4B58C0C (void);
// 0x0000000D System.Void Movement::StartBallMovement()
extern void Movement_StartBallMovement_m4031DDE6F4A746D7503D67DD5E2ABAB62C8815E9 (void);
// 0x0000000E System.Void Movement::MoveBall(UnityEngine.Vector2)
extern void Movement_MoveBall_m401179AC0CB7A39FDC0CC311F1FF444AB8F18207 (void);
// 0x0000000F System.Void Movement::Update()
extern void Movement_Update_m4B99F519DF0A29B476F90FE4314A770CD53EC418 (void);
// 0x00000010 System.Void Movement::IncreaseHitcount()
extern void Movement_IncreaseHitcount_mE90A78AB6FD1CE47F3DB9394EB325D3822C52E03 (void);
// 0x00000011 System.Void Movement::BounceOfRacket(UnityEngine.Collision2D)
extern void Movement_BounceOfRacket_m1ED05CF009B70EF08019F4365719F37F4DFAAE51 (void);
// 0x00000012 System.Void Movement::.ctor()
extern void Movement__ctor_mCB72C1AD57256D73959D74FB86C5D0AA69EAE7ED (void);
// 0x00000013 System.Void Movement::.cctor()
extern void Movement__cctor_m2EFEAE2FF7B92CE5E9F6201EE09E50F7F5C2C063 (void);
// 0x00000014 System.Void Movement/<StartBall>d__8::.ctor(System.Int32)
extern void U3CStartBallU3Ed__8__ctor_m261B94E2B9C4A043F098BBDB74F53723695840D0 (void);
// 0x00000015 System.Void Movement/<StartBall>d__8::System.IDisposable.Dispose()
extern void U3CStartBallU3Ed__8_System_IDisposable_Dispose_m502500679E9B0D038E96FB37164E3C032D4C039F (void);
// 0x00000016 System.Boolean Movement/<StartBall>d__8::MoveNext()
extern void U3CStartBallU3Ed__8_MoveNext_m3C8977747AAB0C248C201B7DE80A0D5A5C7A7A5D (void);
// 0x00000017 System.Object Movement/<StartBall>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartBallU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3055AE4653F7D2E9FCF6999FFF1DC840797C0C0E (void);
// 0x00000018 System.Void Movement/<StartBall>d__8::System.Collections.IEnumerator.Reset()
extern void U3CStartBallU3Ed__8_System_Collections_IEnumerator_Reset_mEE403B945D5DD72576D077D60974CC97F9C58434 (void);
// 0x00000019 System.Object Movement/<StartBall>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CStartBallU3Ed__8_System_Collections_IEnumerator_get_Current_m17187F50319DFF710339D8D4807BAD7A008B1211 (void);
// 0x0000001A System.Void Movement/<StartBallBegin>d__9::.ctor(System.Int32)
extern void U3CStartBallBeginU3Ed__9__ctor_m969FDD0F399392D5CBCECDAFA330423FE7410F94 (void);
// 0x0000001B System.Void Movement/<StartBallBegin>d__9::System.IDisposable.Dispose()
extern void U3CStartBallBeginU3Ed__9_System_IDisposable_Dispose_m6E465B727E782D4668B273DBAD752DF990D6EFC3 (void);
// 0x0000001C System.Boolean Movement/<StartBallBegin>d__9::MoveNext()
extern void U3CStartBallBeginU3Ed__9_MoveNext_m8BF1F990FD57FBF407280DAFE8269EBBD80A06FC (void);
// 0x0000001D System.Object Movement/<StartBallBegin>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartBallBeginU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88A91D9E824D4AADA56EE94F0A12B481FC843D92 (void);
// 0x0000001E System.Void Movement/<StartBallBegin>d__9::System.Collections.IEnumerator.Reset()
extern void U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_Reset_mCF7D52715A3C207596D5B43D4D95A47DA14BC9B3 (void);
// 0x0000001F System.Object Movement/<StartBallBegin>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_get_Current_m60D4831D1BD4E725A5F7132A1973BA2B8A7DED4E (void);
// 0x00000020 System.Void BackgroundTransition::Start()
extern void BackgroundTransition_Start_mBE50171EC60C75E275EED90DA9B15E5B0597003A (void);
// 0x00000021 System.Void BackgroundTransition::FixedUpdate()
extern void BackgroundTransition_FixedUpdate_mD75469167C1F71DAB71AE778D4E46254376D26E4 (void);
// 0x00000022 System.Void BackgroundTransition::SwitchBackground()
extern void BackgroundTransition_SwitchBackground_m4365611A01D2210EBFAC4B710877A1EDAAC881ED (void);
// 0x00000023 System.Void BackgroundTransition::.ctor()
extern void BackgroundTransition__ctor_m1B41119B3DE4A4C511E74CD330BFB20123B78A22 (void);
// 0x00000024 System.Void CamColliderSpawner::Start()
extern void CamColliderSpawner_Start_mAC11221F61A72498BBB18E4E3255D3D1CB0E8FF3 (void);
// 0x00000025 System.Void CamColliderSpawner::.ctor()
extern void CamColliderSpawner__ctor_m4942DC577AA5A874A97AACF1262B0DA8C612979B (void);
// 0x00000026 System.Void CamEnemySpawner::SpawnOpponent()
extern void CamEnemySpawner_SpawnOpponent_m8406402341250595BA549C8F80F5543216BBD3BE (void);
// 0x00000027 System.Void CamEnemySpawner::.ctor()
extern void CamEnemySpawner__ctor_m47ECE271E63EB8FD18442EF8870424418A88B7FF (void);
// 0x00000028 System.Collections.IEnumerator CamShake::Shake(System.Single,System.Single)
extern void CamShake_Shake_m4DFCD0862F86BA37BD72C39979F0FF9C6B94725C (void);
// 0x00000029 System.Void CamShake::.ctor()
extern void CamShake__ctor_m285386FD991E05FD1A913C3D0D15AFC18133DE3E (void);
// 0x0000002A System.Void CamShake/<Shake>d__0::.ctor(System.Int32)
extern void U3CShakeU3Ed__0__ctor_m001A4DAC11F83E040B1737396123EE16533DC115 (void);
// 0x0000002B System.Void CamShake/<Shake>d__0::System.IDisposable.Dispose()
extern void U3CShakeU3Ed__0_System_IDisposable_Dispose_mBDD400FF99EBF54FE198656297D49920B7FC653C (void);
// 0x0000002C System.Boolean CamShake/<Shake>d__0::MoveNext()
extern void U3CShakeU3Ed__0_MoveNext_mBD893168A302DAE130694A4AADCEE800E98E7943 (void);
// 0x0000002D System.Object CamShake/<Shake>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShakeU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2721D362D9022BD217DBBD8BABB9FD8D99991378 (void);
// 0x0000002E System.Void CamShake/<Shake>d__0::System.Collections.IEnumerator.Reset()
extern void U3CShakeU3Ed__0_System_Collections_IEnumerator_Reset_m5B55FE83536B130DF7D1FC51CC60862C08E92BDC (void);
// 0x0000002F System.Object CamShake/<Shake>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CShakeU3Ed__0_System_Collections_IEnumerator_get_Current_mDA94ACFD4A71FB631C5733ECCA3F5C4E4BFA09C5 (void);
// 0x00000030 System.Void CamTransition::Awake()
extern void CamTransition_Awake_m04C384B51116432F1C0B35DE86AD79F63F6A4546 (void);
// 0x00000031 System.Void CamTransition::FixedUpdate()
extern void CamTransition_FixedUpdate_m1988971D8E1C6B0091BC1CD92BB9047B4431D175 (void);
// 0x00000032 System.Void CamTransition::MoveCam()
extern void CamTransition_MoveCam_mCA73AF04A5DB904EE83E45EC802FB233198CF896 (void);
// 0x00000033 System.Void CamTransition::.ctor()
extern void CamTransition__ctor_m61FC656688BB5E5886B4A7A759EB3171D6D1DC50 (void);
// 0x00000034 System.Void EnemyAI::Start()
extern void EnemyAI_Start_mF7B5F2CCA1F4E07B3443D6520DFC0114CCBF7D28 (void);
// 0x00000035 System.Void EnemyAI::Update()
extern void EnemyAI_Update_mD595950351F973E18DF3AB79692E6DE4BDFC16B0 (void);
// 0x00000036 System.Void EnemyAI::Confine()
extern void EnemyAI_Confine_m062DF3FDCE7F93C6B54BE3A871FC37A9F4EE60C2 (void);
// 0x00000037 System.Void EnemyAI::.ctor()
extern void EnemyAI__ctor_m6C8AF5E69E44D32D35FC23F33F01C77FC89ACAE2 (void);
// 0x00000038 System.Void BallAssist::ScoredToLoad()
extern void BallAssist_ScoredToLoad_m1624AFCE8C2C2C1DA9630ABCA2769779BAE4F80E (void);
// 0x00000039 System.Void BallAssist::DifficultyIncrease()
extern void BallAssist_DifficultyIncrease_m957F902F3F1981F75C77306F5530BCF8D0216C0E (void);
// 0x0000003A System.Void BallAssist::.ctor()
extern void BallAssist__ctor_m2D7DEAE0F9E65676B9115429A2CB7C7F10B78069 (void);
// 0x0000003B System.Void ExitToMenu::FixedUpdate()
extern void ExitToMenu_FixedUpdate_m1C6B7722EEB17F7BE8D1D5F20F4E1D099451A9DC (void);
// 0x0000003C System.Collections.IEnumerator ExitToMenu::ClickTime()
extern void ExitToMenu_ClickTime_m002081D65760B8428510CA73F9AF4C4DADEEF4DE (void);
// 0x0000003D System.Void ExitToMenu::GoToMenu()
extern void ExitToMenu_GoToMenu_mF4C2081915E0F9A4E3D18E705761AFF6CD801B0D (void);
// 0x0000003E System.Void ExitToMenu::.ctor()
extern void ExitToMenu__ctor_m4FE3DFCE973129C9C9C256A0AFC5B3DD99B19FC8 (void);
// 0x0000003F System.Void ExitToMenu/<ClickTime>d__3::.ctor(System.Int32)
extern void U3CClickTimeU3Ed__3__ctor_mA7D19888C55D5E63B531BFFCAD3D047C56F82242 (void);
// 0x00000040 System.Void ExitToMenu/<ClickTime>d__3::System.IDisposable.Dispose()
extern void U3CClickTimeU3Ed__3_System_IDisposable_Dispose_m9908CD7316BA316AC21629BC2318B31E181BE3EF (void);
// 0x00000041 System.Boolean ExitToMenu/<ClickTime>d__3::MoveNext()
extern void U3CClickTimeU3Ed__3_MoveNext_m0A7B5B27124BF31D6FFA8A47CF6010834F53E4C2 (void);
// 0x00000042 System.Object ExitToMenu/<ClickTime>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClickTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE51E72E08BA69240C255D0B71078A8CB133C85E1 (void);
// 0x00000043 System.Void ExitToMenu/<ClickTime>d__3::System.Collections.IEnumerator.Reset()
extern void U3CClickTimeU3Ed__3_System_Collections_IEnumerator_Reset_m5982897E3A2982FAE4903E5AEE1CE6A929FD018D (void);
// 0x00000044 System.Object ExitToMenu/<ClickTime>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CClickTimeU3Ed__3_System_Collections_IEnumerator_get_Current_mE79BE7A26E1E5D9171CCC567511A2329738A20BF (void);
// 0x00000045 System.Void GameOver::Start()
extern void GameOver_Start_m69F2EA72A4C2B7A2292169AECAEE630860C57B89 (void);
// 0x00000046 System.Void GameOver::PlayGameOver()
extern void GameOver_PlayGameOver_m2662FED967D970069970A0206D97D336EE916EDD (void);
// 0x00000047 System.Void GameOver::Continue()
extern void GameOver_Continue_m443CAAE6556E9B6B73F80F4CD2E76242AE77FCEB (void);
// 0x00000048 System.Void GameOver::.ctor()
extern void GameOver__ctor_m2D4239F9C4BCE2EBFD1D1D6FAFEBAD05F65399B2 (void);
// 0x00000049 System.Void PauseMenu::Awake()
extern void PauseMenu_Awake_mD17C2664126E61F76112D3C96B5A4684E0D00B66 (void);
// 0x0000004A System.Void PauseMenu::Resume()
extern void PauseMenu_Resume_mA45D6E4F27172EF775FC2D3F694BE90882DBE831 (void);
// 0x0000004B System.Void PauseMenu::Pause()
extern void PauseMenu_Pause_m5C6B949CB19184E7227C891FF3C8AF707689AAF0 (void);
// 0x0000004C System.Void PauseMenu::GameOverResume()
extern void PauseMenu_GameOverResume_mFE24251D222CE8193D9F7552810F2087E84045F6 (void);
// 0x0000004D System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_m81B0E020DC5008DA4D414200BAAF7122B430D826 (void);
// 0x0000004E System.Void PauseMenu::.cctor()
extern void PauseMenu__cctor_mDA28AAD1F51985A60F893A51E3C1C2057F9FC662 (void);
// 0x0000004F System.Void QualityManager::Start()
extern void QualityManager_Start_mC31E5BA7DA82AB03AF89DE62FD9601CCABCB29B6 (void);
// 0x00000050 System.Void QualityManager::SetHighQuality()
extern void QualityManager_SetHighQuality_m1D43F1D17D6A1F723569C6585DE5CB35EB9EC9EB (void);
// 0x00000051 System.Void QualityManager::SetLowQuality()
extern void QualityManager_SetLowQuality_m6A5B20CCC31240C020A253B741C00421A4FBD549 (void);
// 0x00000052 System.Void QualityManager::MusicOn()
extern void QualityManager_MusicOn_m24AD817BDC757D37223BF03CAEA74561E75F9F15 (void);
// 0x00000053 System.Void QualityManager::MusicOff()
extern void QualityManager_MusicOff_m7F71FFF447B393B6B11BDB6ABB8202BA0A467AA5 (void);
// 0x00000054 System.Void QualityManager::CheckQuality()
extern void QualityManager_CheckQuality_m3FA175564D739F4ABE021CE3E01719DDA14AE500 (void);
// 0x00000055 System.Void QualityManager::.ctor()
extern void QualityManager__ctor_m5D8A79BF25DCC09D2E3F0F22436B73FE01C0AE9E (void);
// 0x00000056 System.Void QualityManager::.cctor()
extern void QualityManager__cctor_mED5D6A013E04D226681B6B52CDB44288A3E3467F (void);
// 0x00000057 System.Void SavingData::Start()
extern void SavingData_Start_m70799380464BC4C38E6BA50E5B5837321F4ABC9A (void);
// 0x00000058 System.Void SavingData::FetchData()
extern void SavingData_FetchData_m2EB150FDC254CE17391725D6C8810B5C1569B125 (void);
// 0x00000059 System.Void SavingData::SaveData()
extern void SavingData_SaveData_mCD000FBFC947CBD83AFACB9673CDCEA4EA402625 (void);
// 0x0000005A System.Void SavingData::.ctor()
extern void SavingData__ctor_mE15D442C43D950BEA1999226774A7B90098770D0 (void);
// 0x0000005B System.Void SceneChanger::LoadMenu()
extern void SceneChanger_LoadMenu_mA536E0672DD47ED525E690F327DB3788B610D850 (void);
// 0x0000005C System.Collections.IEnumerator SceneChanger::LoadLevel(System.Int32)
extern void SceneChanger_LoadLevel_mCBAD8587143ECC718DEB6DD442234ED1232680B6 (void);
// 0x0000005D System.Void SceneChanger::.ctor()
extern void SceneChanger__ctor_mE9DB2B4102825D23A4B0271C2A6023F02920B4AD (void);
// 0x0000005E System.Void SceneChanger/<LoadLevel>d__4::.ctor(System.Int32)
extern void U3CLoadLevelU3Ed__4__ctor_m0118D10A4A9DA32B67B26D83475D4C3125301D3B (void);
// 0x0000005F System.Void SceneChanger/<LoadLevel>d__4::System.IDisposable.Dispose()
extern void U3CLoadLevelU3Ed__4_System_IDisposable_Dispose_m6DDF95A1D5700326A1B9AE11841C04013DD0F4C5 (void);
// 0x00000060 System.Boolean SceneChanger/<LoadLevel>d__4::MoveNext()
extern void U3CLoadLevelU3Ed__4_MoveNext_m08C51F0F41A379880992ADC942F22523BEF1A514 (void);
// 0x00000061 System.Object SceneChanger/<LoadLevel>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLevelU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34EC7B84E5BB1E165B4BF0F0C495C3A89F1260C5 (void);
// 0x00000062 System.Void SceneChanger/<LoadLevel>d__4::System.Collections.IEnumerator.Reset()
extern void U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_Reset_m1100C08340B30B7B9DE3FA79E02DA470E20A13E1 (void);
// 0x00000063 System.Object SceneChanger/<LoadLevel>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_get_Current_m9BD5A022996FFE7F33EA905E470091ADADC0934D (void);
// 0x00000064 System.Void ScoreController::Start()
extern void ScoreController_Start_m1D6585F7BBBC730961CFB8BACC546A124F0D9646 (void);
// 0x00000065 System.Void ScoreController::FixedUpdate()
extern void ScoreController_FixedUpdate_mB037EEA73A7EF4CEAFB7F651EE9D5169A564D556 (void);
// 0x00000066 System.Void ScoreController::Scored()
extern void ScoreController_Scored_mAD98FE74722A4012393D3A6052E527B4FAE78B53 (void);
// 0x00000067 System.Void ScoreController::.ctor()
extern void ScoreController__ctor_m161DBF9A2ED8098CD117A438518B3C716F0BC5F7 (void);
// 0x00000068 UnityEngine.GameObject EnemyFinder::FindClosestEnemy()
extern void EnemyFinder_FindClosestEnemy_m504513D62CF5EE17627D57C6CE8B8DBEA9DDF7B3 (void);
// 0x00000069 System.Void EnemyFinder::EnemyDed()
extern void EnemyFinder_EnemyDed_mC71E12974E02192328562E530AAFB9CF2C87D55A (void);
// 0x0000006A System.Void EnemyFinder::.ctor()
extern void EnemyFinder__ctor_m2C31597E0372CBF2A71EF0A0C28CC6325D880FE4 (void);
// 0x0000006B System.Void PlayerController::Update()
extern void PlayerController_Update_m1F4051EB5BCBCCE5EEE2E3E49B7E278C3B14EC33 (void);
// 0x0000006C System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33 (void);
// 0x0000006D System.Void PlayerController::.cctor()
extern void PlayerController__cctor_m1FA67A4FD0B3B6880EBFB160ABC729E8A37834F4 (void);
// 0x0000006E System.Void PlayerData::.ctor(SavingData)
extern void PlayerData__ctor_mC9F798B4DE17ED52FBB352F33C99C309EB624B29 (void);
// 0x0000006F System.Void SaveSystem::SaveData(SavingData)
extern void SaveSystem_SaveData_m5057FD263203AD9BAB419D6D3E034CAA10BA23BB (void);
// 0x00000070 PlayerData SaveSystem::LoadData()
extern void SaveSystem_LoadData_m35E88A21A3BB2E2949D63F1D4928B2A2440F7AA8 (void);
// 0x00000071 System.Void BuyWithAds::PlayAds()
extern void BuyWithAds_PlayAds_m346B104EA5123DC83ACC677EBFA341C5E919D982 (void);
// 0x00000072 System.Void BuyWithAds::EndAds()
extern void BuyWithAds_EndAds_m6D2534A5B10FB12D011F4C0D16CC550D2A3A0030 (void);
// 0x00000073 System.Void BuyWithAds::.ctor()
extern void BuyWithAds__ctor_mA08E40E09EAC44FDDA7731EB4E38EF24B30548A0 (void);
// 0x00000074 System.Void SettingsAnimControl::MenuTrigger()
extern void SettingsAnimControl_MenuTrigger_m94571B5CB4EC4130F3C4737D8AA6A4B37EF6A52A (void);
// 0x00000075 System.Void SettingsAnimControl::OpenMenu()
extern void SettingsAnimControl_OpenMenu_m64666BBFA09BBDCAB36657897D54D523BA703273 (void);
// 0x00000076 System.Void SettingsAnimControl::CloseMenu()
extern void SettingsAnimControl_CloseMenu_m38B242C767C32CA10F57EF8557540C6292C666B2 (void);
// 0x00000077 System.Void SettingsAnimControl::.ctor()
extern void SettingsAnimControl__ctor_m6EB5CDE688CE4CBE71421D7F95FFD4C26178FFF0 (void);
// 0x00000078 System.Void ExitGame::FixedUpdate()
extern void ExitGame_FixedUpdate_m58B9A2E9681B2CDA70B62362A4501EC59C4A7C5F (void);
// 0x00000079 System.Collections.IEnumerator ExitGame::ClickTime()
extern void ExitGame_ClickTime_m3CB8235A53517FEB5A60F96EDD5F1948442D4FF0 (void);
// 0x0000007A System.Void ExitGame::.ctor()
extern void ExitGame__ctor_mF031F846CC7DE286155C56931B1D99E3B60E9151 (void);
// 0x0000007B System.Void ExitGame/<ClickTime>d__2::.ctor(System.Int32)
extern void U3CClickTimeU3Ed__2__ctor_m10F0E090C5B0F2A1C625BBC99C92C6D22D31BCCE (void);
// 0x0000007C System.Void ExitGame/<ClickTime>d__2::System.IDisposable.Dispose()
extern void U3CClickTimeU3Ed__2_System_IDisposable_Dispose_mDEC414CF9AD02F52B58364BC65113AEE5EFBB448 (void);
// 0x0000007D System.Boolean ExitGame/<ClickTime>d__2::MoveNext()
extern void U3CClickTimeU3Ed__2_MoveNext_m9B1B78C813B8399599B5D5A2AA15888BF440799A (void);
// 0x0000007E System.Object ExitGame/<ClickTime>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClickTimeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D46A4DF9B833C2B48CECC6DF8AF88EB6B74937B (void);
// 0x0000007F System.Void ExitGame/<ClickTime>d__2::System.Collections.IEnumerator.Reset()
extern void U3CClickTimeU3Ed__2_System_Collections_IEnumerator_Reset_mB3CB1196BB9D97C5523B8924EDDB1D529FA3E3AE (void);
// 0x00000080 System.Object ExitGame/<ClickTime>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CClickTimeU3Ed__2_System_Collections_IEnumerator_get_Current_m3B0FD487BA83CA9DBD473F25B6E36EDB0FD3A95B (void);
// 0x00000081 System.Void HighScoreManager::Start()
extern void HighScoreManager_Start_m47F53BC4376EAD00C4A7461219FE5063A4441C9D (void);
// 0x00000082 System.Void HighScoreManager::LoadData()
extern void HighScoreManager_LoadData_mBBD7D0B64B608867177BBA49BC150B6A785BEB46 (void);
// 0x00000083 System.Void HighScoreManager::Reset()
extern void HighScoreManager_Reset_m09A446FC4D64537252134CE84A17F1F83877B20C (void);
// 0x00000084 System.Void HighScoreManager::.ctor()
extern void HighScoreManager__ctor_m3E3C2D89CF5E838C760F9A54B3DF6B5A3F5438D7 (void);
// 0x00000085 System.Void SceneSwitcher::SwitchLeft()
extern void SceneSwitcher_SwitchLeft_mDCEB368AB9E34320CBC66E98FFE96DB3161B46A0 (void);
// 0x00000086 System.Void SceneSwitcher::SwitchRight()
extern void SceneSwitcher_SwitchRight_mC6F3BA0601893881DAA95FF45E8BEE20AB469026 (void);
// 0x00000087 System.Void SceneSwitcher::PlayGame(System.String)
extern void SceneSwitcher_PlayGame_m53A17CDD95B058990792120F796C9EBA36C5F81C (void);
// 0x00000088 System.Collections.IEnumerator SceneSwitcher::LoadLevel(System.Int32)
extern void SceneSwitcher_LoadLevel_m3EA5A40FB32E6DF6D886755907E64C4B8CF062C3 (void);
// 0x00000089 System.Collections.IEnumerator SceneSwitcher::LoadLevelwName(System.String)
extern void SceneSwitcher_LoadLevelwName_m2300EB1146D8857C3DF0B0FB176B22E84C52D53A (void);
// 0x0000008A System.Void SceneSwitcher::.ctor()
extern void SceneSwitcher__ctor_m286A74582BD4C6059DACBC69C75EE335165E73DA (void);
// 0x0000008B System.Void SceneSwitcher/<LoadLevel>d__5::.ctor(System.Int32)
extern void U3CLoadLevelU3Ed__5__ctor_m7DACC46708AFF1BD60236D7EEA9ABF79ADD61E19 (void);
// 0x0000008C System.Void SceneSwitcher/<LoadLevel>d__5::System.IDisposable.Dispose()
extern void U3CLoadLevelU3Ed__5_System_IDisposable_Dispose_m09DDD62A5E9285812930490BA0197E8AC2766AAF (void);
// 0x0000008D System.Boolean SceneSwitcher/<LoadLevel>d__5::MoveNext()
extern void U3CLoadLevelU3Ed__5_MoveNext_m82E58802EF99273D9DE12ACAF598DA9BF0BE5BFE (void);
// 0x0000008E System.Object SceneSwitcher/<LoadLevel>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLevelU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FACEAE81034CE7385D3BEC98F6F397DB33E8481 (void);
// 0x0000008F System.Void SceneSwitcher/<LoadLevel>d__5::System.Collections.IEnumerator.Reset()
extern void U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_Reset_mFE9DBAA96F09F0570B62DC9EE53F9FDA7DCD57FB (void);
// 0x00000090 System.Object SceneSwitcher/<LoadLevel>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_get_Current_m353AE2CDC1240882262C09E4EE7B34E12A5E07ED (void);
// 0x00000091 System.Void SceneSwitcher/<LoadLevelwName>d__6::.ctor(System.Int32)
extern void U3CLoadLevelwNameU3Ed__6__ctor_mB15009D33D5357759FB42ED830249ECEF330A8EA (void);
// 0x00000092 System.Void SceneSwitcher/<LoadLevelwName>d__6::System.IDisposable.Dispose()
extern void U3CLoadLevelwNameU3Ed__6_System_IDisposable_Dispose_mE1D869755F1445B08F4DC219C6E6DA54E284CACE (void);
// 0x00000093 System.Boolean SceneSwitcher/<LoadLevelwName>d__6::MoveNext()
extern void U3CLoadLevelwNameU3Ed__6_MoveNext_mFB2E240672940DE8FCAE51B955387482BC5EEB78 (void);
// 0x00000094 System.Object SceneSwitcher/<LoadLevelwName>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLevelwNameU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25CFE96CE428B8983189044A32B66A0D5CE3301F (void);
// 0x00000095 System.Void SceneSwitcher/<LoadLevelwName>d__6::System.Collections.IEnumerator.Reset()
extern void U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_Reset_m270E6BBD18C9D623264B1A1F8FD25CDCFDF69C31 (void);
// 0x00000096 System.Object SceneSwitcher/<LoadLevelwName>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_get_Current_m71270DB4FECBBE577D7EC2655BDDAF314A01A5BB (void);
// 0x00000097 System.Void ThemeManager::Start()
extern void ThemeManager_Start_m2E92EE8EA529B42E7E0B4B2E9EF50F8E000C245F (void);
// 0x00000098 System.Void ThemeManager::ThemeSwitch(System.Boolean)
extern void ThemeManager_ThemeSwitch_mC87350E5894F4B0DAB570C4515CCB49A5A02B6D7 (void);
// 0x00000099 System.Void ThemeManager::.ctor()
extern void ThemeManager__ctor_m1B15A16D3C500F39D01E5B302126CC3BFC819FF7 (void);
// 0x0000009A System.Void Resetter::Start()
extern void Resetter_Start_m8DFC9D64D7FF65B33169EAF3C8F367D534A59637 (void);
// 0x0000009B System.Void Resetter::.ctor()
extern void Resetter__ctor_m6C9B56DD044943B392BA40A49CE40AA56800DDE8 (void);
// 0x0000009C System.Void Ball::.ctor()
extern void Ball__ctor_mDFF93D8FA13BD70DF4F2FA2B9B402E3B10BD6EDC (void);
// 0x0000009D System.Void StartMainMenu::GoToMainMenu()
extern void StartMainMenu_GoToMainMenu_mC83CC842E9993AB6E39610288B991A778AD14DEE (void);
// 0x0000009E System.Void StartMainMenu::.ctor()
extern void StartMainMenu__ctor_mD47012B01FD74BD8C0B2DF7C8D6B8CFBFB26872F (void);
static Il2CppMethodPointer s_methodPointers[158] = 
{
	SoundEffectsPlayer_ballHit_mB7175961660F44CA08B0D59C46A998DDD9BC8AE3,
	SoundEffectsPlayer_pointGainedSound_mB4F2893405AA1D5EEA89A0EA64853AA3F958DCB4,
	SoundEffectsPlayer__ctor_m7CC4A5526F138EED802681BB3E482E03EDA5A0D7,
	BallDisplay_Start_m3253F4F280159847A3F6E18C7F806F2DB78FD94E,
	BallDisplay__ctor_mD74AF876C1888FD81F9451E10EB7C4F3F0CC3BB6,
	CollisionControl_Start_m94E3FF19E61F65014148879191D5C21EE6C50AB8,
	CollisionControl_OnCollisionEnter2D_m4DD0B090C017B2C7B21100AAB2EEEA6F65E7C2E5,
	CollisionControl_OnTriggerEnter2D_m5369B8FE04ECB8EE602150EEDB740349BF03625E,
	CollisionControl__ctor_mF95690D06C9CA4A70B46CCEE2BCD1D0209404E8F,
	Movement_Start_m3D09153FD03F06C31BBD151C21BA361EA49FA72B,
	Movement_StartBall_m8653974505239385E5EC1A085953760A71BA06CE,
	Movement_StartBallBegin_mB4BFD1CCD96D9512D37B1560658928D5E4B58C0C,
	Movement_StartBallMovement_m4031DDE6F4A746D7503D67DD5E2ABAB62C8815E9,
	Movement_MoveBall_m401179AC0CB7A39FDC0CC311F1FF444AB8F18207,
	Movement_Update_m4B99F519DF0A29B476F90FE4314A770CD53EC418,
	Movement_IncreaseHitcount_mE90A78AB6FD1CE47F3DB9394EB325D3822C52E03,
	Movement_BounceOfRacket_m1ED05CF009B70EF08019F4365719F37F4DFAAE51,
	Movement__ctor_mCB72C1AD57256D73959D74FB86C5D0AA69EAE7ED,
	Movement__cctor_m2EFEAE2FF7B92CE5E9F6201EE09E50F7F5C2C063,
	U3CStartBallU3Ed__8__ctor_m261B94E2B9C4A043F098BBDB74F53723695840D0,
	U3CStartBallU3Ed__8_System_IDisposable_Dispose_m502500679E9B0D038E96FB37164E3C032D4C039F,
	U3CStartBallU3Ed__8_MoveNext_m3C8977747AAB0C248C201B7DE80A0D5A5C7A7A5D,
	U3CStartBallU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3055AE4653F7D2E9FCF6999FFF1DC840797C0C0E,
	U3CStartBallU3Ed__8_System_Collections_IEnumerator_Reset_mEE403B945D5DD72576D077D60974CC97F9C58434,
	U3CStartBallU3Ed__8_System_Collections_IEnumerator_get_Current_m17187F50319DFF710339D8D4807BAD7A008B1211,
	U3CStartBallBeginU3Ed__9__ctor_m969FDD0F399392D5CBCECDAFA330423FE7410F94,
	U3CStartBallBeginU3Ed__9_System_IDisposable_Dispose_m6E465B727E782D4668B273DBAD752DF990D6EFC3,
	U3CStartBallBeginU3Ed__9_MoveNext_m8BF1F990FD57FBF407280DAFE8269EBBD80A06FC,
	U3CStartBallBeginU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88A91D9E824D4AADA56EE94F0A12B481FC843D92,
	U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_Reset_mCF7D52715A3C207596D5B43D4D95A47DA14BC9B3,
	U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_get_Current_m60D4831D1BD4E725A5F7132A1973BA2B8A7DED4E,
	BackgroundTransition_Start_mBE50171EC60C75E275EED90DA9B15E5B0597003A,
	BackgroundTransition_FixedUpdate_mD75469167C1F71DAB71AE778D4E46254376D26E4,
	BackgroundTransition_SwitchBackground_m4365611A01D2210EBFAC4B710877A1EDAAC881ED,
	BackgroundTransition__ctor_m1B41119B3DE4A4C511E74CD330BFB20123B78A22,
	CamColliderSpawner_Start_mAC11221F61A72498BBB18E4E3255D3D1CB0E8FF3,
	CamColliderSpawner__ctor_m4942DC577AA5A874A97AACF1262B0DA8C612979B,
	CamEnemySpawner_SpawnOpponent_m8406402341250595BA549C8F80F5543216BBD3BE,
	CamEnemySpawner__ctor_m47ECE271E63EB8FD18442EF8870424418A88B7FF,
	CamShake_Shake_m4DFCD0862F86BA37BD72C39979F0FF9C6B94725C,
	CamShake__ctor_m285386FD991E05FD1A913C3D0D15AFC18133DE3E,
	U3CShakeU3Ed__0__ctor_m001A4DAC11F83E040B1737396123EE16533DC115,
	U3CShakeU3Ed__0_System_IDisposable_Dispose_mBDD400FF99EBF54FE198656297D49920B7FC653C,
	U3CShakeU3Ed__0_MoveNext_mBD893168A302DAE130694A4AADCEE800E98E7943,
	U3CShakeU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2721D362D9022BD217DBBD8BABB9FD8D99991378,
	U3CShakeU3Ed__0_System_Collections_IEnumerator_Reset_m5B55FE83536B130DF7D1FC51CC60862C08E92BDC,
	U3CShakeU3Ed__0_System_Collections_IEnumerator_get_Current_mDA94ACFD4A71FB631C5733ECCA3F5C4E4BFA09C5,
	CamTransition_Awake_m04C384B51116432F1C0B35DE86AD79F63F6A4546,
	CamTransition_FixedUpdate_m1988971D8E1C6B0091BC1CD92BB9047B4431D175,
	CamTransition_MoveCam_mCA73AF04A5DB904EE83E45EC802FB233198CF896,
	CamTransition__ctor_m61FC656688BB5E5886B4A7A759EB3171D6D1DC50,
	EnemyAI_Start_mF7B5F2CCA1F4E07B3443D6520DFC0114CCBF7D28,
	EnemyAI_Update_mD595950351F973E18DF3AB79692E6DE4BDFC16B0,
	EnemyAI_Confine_m062DF3FDCE7F93C6B54BE3A871FC37A9F4EE60C2,
	EnemyAI__ctor_m6C8AF5E69E44D32D35FC23F33F01C77FC89ACAE2,
	BallAssist_ScoredToLoad_m1624AFCE8C2C2C1DA9630ABCA2769779BAE4F80E,
	BallAssist_DifficultyIncrease_m957F902F3F1981F75C77306F5530BCF8D0216C0E,
	BallAssist__ctor_m2D7DEAE0F9E65676B9115429A2CB7C7F10B78069,
	ExitToMenu_FixedUpdate_m1C6B7722EEB17F7BE8D1D5F20F4E1D099451A9DC,
	ExitToMenu_ClickTime_m002081D65760B8428510CA73F9AF4C4DADEEF4DE,
	ExitToMenu_GoToMenu_mF4C2081915E0F9A4E3D18E705761AFF6CD801B0D,
	ExitToMenu__ctor_m4FE3DFCE973129C9C9C256A0AFC5B3DD99B19FC8,
	U3CClickTimeU3Ed__3__ctor_mA7D19888C55D5E63B531BFFCAD3D047C56F82242,
	U3CClickTimeU3Ed__3_System_IDisposable_Dispose_m9908CD7316BA316AC21629BC2318B31E181BE3EF,
	U3CClickTimeU3Ed__3_MoveNext_m0A7B5B27124BF31D6FFA8A47CF6010834F53E4C2,
	U3CClickTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE51E72E08BA69240C255D0B71078A8CB133C85E1,
	U3CClickTimeU3Ed__3_System_Collections_IEnumerator_Reset_m5982897E3A2982FAE4903E5AEE1CE6A929FD018D,
	U3CClickTimeU3Ed__3_System_Collections_IEnumerator_get_Current_mE79BE7A26E1E5D9171CCC567511A2329738A20BF,
	GameOver_Start_m69F2EA72A4C2B7A2292169AECAEE630860C57B89,
	GameOver_PlayGameOver_m2662FED967D970069970A0206D97D336EE916EDD,
	GameOver_Continue_m443CAAE6556E9B6B73F80F4CD2E76242AE77FCEB,
	GameOver__ctor_m2D4239F9C4BCE2EBFD1D1D6FAFEBAD05F65399B2,
	PauseMenu_Awake_mD17C2664126E61F76112D3C96B5A4684E0D00B66,
	PauseMenu_Resume_mA45D6E4F27172EF775FC2D3F694BE90882DBE831,
	PauseMenu_Pause_m5C6B949CB19184E7227C891FF3C8AF707689AAF0,
	PauseMenu_GameOverResume_mFE24251D222CE8193D9F7552810F2087E84045F6,
	PauseMenu__ctor_m81B0E020DC5008DA4D414200BAAF7122B430D826,
	PauseMenu__cctor_mDA28AAD1F51985A60F893A51E3C1C2057F9FC662,
	QualityManager_Start_mC31E5BA7DA82AB03AF89DE62FD9601CCABCB29B6,
	QualityManager_SetHighQuality_m1D43F1D17D6A1F723569C6585DE5CB35EB9EC9EB,
	QualityManager_SetLowQuality_m6A5B20CCC31240C020A253B741C00421A4FBD549,
	QualityManager_MusicOn_m24AD817BDC757D37223BF03CAEA74561E75F9F15,
	QualityManager_MusicOff_m7F71FFF447B393B6B11BDB6ABB8202BA0A467AA5,
	QualityManager_CheckQuality_m3FA175564D739F4ABE021CE3E01719DDA14AE500,
	QualityManager__ctor_m5D8A79BF25DCC09D2E3F0F22436B73FE01C0AE9E,
	QualityManager__cctor_mED5D6A013E04D226681B6B52CDB44288A3E3467F,
	SavingData_Start_m70799380464BC4C38E6BA50E5B5837321F4ABC9A,
	SavingData_FetchData_m2EB150FDC254CE17391725D6C8810B5C1569B125,
	SavingData_SaveData_mCD000FBFC947CBD83AFACB9673CDCEA4EA402625,
	SavingData__ctor_mE15D442C43D950BEA1999226774A7B90098770D0,
	SceneChanger_LoadMenu_mA536E0672DD47ED525E690F327DB3788B610D850,
	SceneChanger_LoadLevel_mCBAD8587143ECC718DEB6DD442234ED1232680B6,
	SceneChanger__ctor_mE9DB2B4102825D23A4B0271C2A6023F02920B4AD,
	U3CLoadLevelU3Ed__4__ctor_m0118D10A4A9DA32B67B26D83475D4C3125301D3B,
	U3CLoadLevelU3Ed__4_System_IDisposable_Dispose_m6DDF95A1D5700326A1B9AE11841C04013DD0F4C5,
	U3CLoadLevelU3Ed__4_MoveNext_m08C51F0F41A379880992ADC942F22523BEF1A514,
	U3CLoadLevelU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34EC7B84E5BB1E165B4BF0F0C495C3A89F1260C5,
	U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_Reset_m1100C08340B30B7B9DE3FA79E02DA470E20A13E1,
	U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_get_Current_m9BD5A022996FFE7F33EA905E470091ADADC0934D,
	ScoreController_Start_m1D6585F7BBBC730961CFB8BACC546A124F0D9646,
	ScoreController_FixedUpdate_mB037EEA73A7EF4CEAFB7F651EE9D5169A564D556,
	ScoreController_Scored_mAD98FE74722A4012393D3A6052E527B4FAE78B53,
	ScoreController__ctor_m161DBF9A2ED8098CD117A438518B3C716F0BC5F7,
	EnemyFinder_FindClosestEnemy_m504513D62CF5EE17627D57C6CE8B8DBEA9DDF7B3,
	EnemyFinder_EnemyDed_mC71E12974E02192328562E530AAFB9CF2C87D55A,
	EnemyFinder__ctor_m2C31597E0372CBF2A71EF0A0C28CC6325D880FE4,
	PlayerController_Update_m1F4051EB5BCBCCE5EEE2E3E49B7E278C3B14EC33,
	PlayerController__ctor_mDDAB7C7D82E1A5B3E6C197B1AB9D653DFE554F33,
	PlayerController__cctor_m1FA67A4FD0B3B6880EBFB160ABC729E8A37834F4,
	PlayerData__ctor_mC9F798B4DE17ED52FBB352F33C99C309EB624B29,
	SaveSystem_SaveData_m5057FD263203AD9BAB419D6D3E034CAA10BA23BB,
	SaveSystem_LoadData_m35E88A21A3BB2E2949D63F1D4928B2A2440F7AA8,
	BuyWithAds_PlayAds_m346B104EA5123DC83ACC677EBFA341C5E919D982,
	BuyWithAds_EndAds_m6D2534A5B10FB12D011F4C0D16CC550D2A3A0030,
	BuyWithAds__ctor_mA08E40E09EAC44FDDA7731EB4E38EF24B30548A0,
	SettingsAnimControl_MenuTrigger_m94571B5CB4EC4130F3C4737D8AA6A4B37EF6A52A,
	SettingsAnimControl_OpenMenu_m64666BBFA09BBDCAB36657897D54D523BA703273,
	SettingsAnimControl_CloseMenu_m38B242C767C32CA10F57EF8557540C6292C666B2,
	SettingsAnimControl__ctor_m6EB5CDE688CE4CBE71421D7F95FFD4C26178FFF0,
	ExitGame_FixedUpdate_m58B9A2E9681B2CDA70B62362A4501EC59C4A7C5F,
	ExitGame_ClickTime_m3CB8235A53517FEB5A60F96EDD5F1948442D4FF0,
	ExitGame__ctor_mF031F846CC7DE286155C56931B1D99E3B60E9151,
	U3CClickTimeU3Ed__2__ctor_m10F0E090C5B0F2A1C625BBC99C92C6D22D31BCCE,
	U3CClickTimeU3Ed__2_System_IDisposable_Dispose_mDEC414CF9AD02F52B58364BC65113AEE5EFBB448,
	U3CClickTimeU3Ed__2_MoveNext_m9B1B78C813B8399599B5D5A2AA15888BF440799A,
	U3CClickTimeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D46A4DF9B833C2B48CECC6DF8AF88EB6B74937B,
	U3CClickTimeU3Ed__2_System_Collections_IEnumerator_Reset_mB3CB1196BB9D97C5523B8924EDDB1D529FA3E3AE,
	U3CClickTimeU3Ed__2_System_Collections_IEnumerator_get_Current_m3B0FD487BA83CA9DBD473F25B6E36EDB0FD3A95B,
	HighScoreManager_Start_m47F53BC4376EAD00C4A7461219FE5063A4441C9D,
	HighScoreManager_LoadData_mBBD7D0B64B608867177BBA49BC150B6A785BEB46,
	HighScoreManager_Reset_m09A446FC4D64537252134CE84A17F1F83877B20C,
	HighScoreManager__ctor_m3E3C2D89CF5E838C760F9A54B3DF6B5A3F5438D7,
	SceneSwitcher_SwitchLeft_mDCEB368AB9E34320CBC66E98FFE96DB3161B46A0,
	SceneSwitcher_SwitchRight_mC6F3BA0601893881DAA95FF45E8BEE20AB469026,
	SceneSwitcher_PlayGame_m53A17CDD95B058990792120F796C9EBA36C5F81C,
	SceneSwitcher_LoadLevel_m3EA5A40FB32E6DF6D886755907E64C4B8CF062C3,
	SceneSwitcher_LoadLevelwName_m2300EB1146D8857C3DF0B0FB176B22E84C52D53A,
	SceneSwitcher__ctor_m286A74582BD4C6059DACBC69C75EE335165E73DA,
	U3CLoadLevelU3Ed__5__ctor_m7DACC46708AFF1BD60236D7EEA9ABF79ADD61E19,
	U3CLoadLevelU3Ed__5_System_IDisposable_Dispose_m09DDD62A5E9285812930490BA0197E8AC2766AAF,
	U3CLoadLevelU3Ed__5_MoveNext_m82E58802EF99273D9DE12ACAF598DA9BF0BE5BFE,
	U3CLoadLevelU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4FACEAE81034CE7385D3BEC98F6F397DB33E8481,
	U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_Reset_mFE9DBAA96F09F0570B62DC9EE53F9FDA7DCD57FB,
	U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_get_Current_m353AE2CDC1240882262C09E4EE7B34E12A5E07ED,
	U3CLoadLevelwNameU3Ed__6__ctor_mB15009D33D5357759FB42ED830249ECEF330A8EA,
	U3CLoadLevelwNameU3Ed__6_System_IDisposable_Dispose_mE1D869755F1445B08F4DC219C6E6DA54E284CACE,
	U3CLoadLevelwNameU3Ed__6_MoveNext_mFB2E240672940DE8FCAE51B955387482BC5EEB78,
	U3CLoadLevelwNameU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25CFE96CE428B8983189044A32B66A0D5CE3301F,
	U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_Reset_m270E6BBD18C9D623264B1A1F8FD25CDCFDF69C31,
	U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_get_Current_m71270DB4FECBBE577D7EC2655BDDAF314A01A5BB,
	ThemeManager_Start_m2E92EE8EA529B42E7E0B4B2E9EF50F8E000C245F,
	ThemeManager_ThemeSwitch_mC87350E5894F4B0DAB570C4515CCB49A5A02B6D7,
	ThemeManager__ctor_m1B15A16D3C500F39D01E5B302126CC3BFC819FF7,
	Resetter_Start_m8DFC9D64D7FF65B33169EAF3C8F367D534A59637,
	Resetter__ctor_m6C9B56DD044943B392BA40A49CE40AA56800DDE8,
	Ball__ctor_mDFF93D8FA13BD70DF4F2FA2B9B402E3B10BD6EDC,
	StartMainMenu_GoToMainMenu_mC83CC842E9993AB6E39610288B991A778AD14DEE,
	StartMainMenu__ctor_mD47012B01FD74BD8C0B2DF7C8D6B8CFBFB26872F,
};
static const int32_t s_InvokerIndices[158] = 
{
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	2966,
	2966,
	3673,
	3673,
	3580,
	3580,
	3673,
	3040,
	3673,
	3673,
	2966,
	3673,
	5622,
	2947,
	3673,
	3517,
	3580,
	3673,
	3580,
	2947,
	3673,
	3517,
	3580,
	3673,
	3580,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	1296,
	3673,
	2947,
	3673,
	3517,
	3580,
	3673,
	3580,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3580,
	3673,
	3673,
	2947,
	3673,
	3517,
	3580,
	3673,
	3580,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	5622,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	5622,
	3673,
	3673,
	3673,
	3673,
	3673,
	2612,
	3673,
	2947,
	3673,
	3517,
	3580,
	3673,
	3580,
	3673,
	3673,
	3673,
	3673,
	3580,
	3673,
	3673,
	3673,
	3673,
	5622,
	2966,
	5506,
	5593,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	3580,
	3673,
	2947,
	3673,
	3517,
	3580,
	3673,
	3580,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
	2966,
	2612,
	2615,
	3673,
	2947,
	3673,
	3517,
	3580,
	3673,
	3580,
	2947,
	3673,
	3517,
	3580,
	3673,
	3580,
	3673,
	2900,
	3673,
	3673,
	3673,
	3673,
	3673,
	3673,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	158,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
