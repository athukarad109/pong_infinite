﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void CollisionControl_tD0D618A5A6C7164DC8D6A61123950D61038854EC_CustomAttributesCacheGenerator_movement(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x75\x6E\x63\x74\x69\x6F\x6E\x20\x49\x6E\x66\x6F\x20\x3A"), NULL);
	}
}
static void CollisionControl_tD0D618A5A6C7164DC8D6A61123950D61038854EC_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61\x20\x53\x68\x61\x6B\x65\x20\x3A"), NULL);
	}
}
static void CollisionControl_tD0D618A5A6C7164DC8D6A61123950D61038854EC_CustomAttributesCacheGenerator_effects(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x45\x66\x66\x65\x63\x74\x73"), NULL);
	}
}
static void CollisionControl_tD0D618A5A6C7164DC8D6A61123950D61038854EC_CustomAttributesCacheGenerator_CoffeeSpawner(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x66\x66\x65\x65"), NULL);
	}
}
static void Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_Movement_StartBall_m7AB43B4E9ED20C2A410DDC75B67F091DD784D0F8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_0_0_0_var), NULL);
	}
}
static void Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_Movement_StartBallBegin_mC9FD9E1D18F5391F7DDDFA4F1E0FF4B2C76F3D7E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_0_0_0_var), NULL);
	}
}
static void U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8__ctor_m97E2DD4D209082B6F7181E6D94703AA2500E3EB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8_System_IDisposable_Dispose_mCEC7941AF2B3549148AF1A1179B92EFD13C5A921(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A4EEACF2C3D6C0E6D266C22977FEE42495B5938(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8_System_Collections_IEnumerator_Reset_mAE7E8A68110AFF3F722E7836993E410502A78AA8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8_System_Collections_IEnumerator_get_Current_m8683BEB4304E01DAC26D210346EF461C7D038388(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9__ctor_m6426507D7E7C9AD7CCFD1882FD1452B43B1EB16B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9_System_IDisposable_Dispose_m573A70317648DC9FC8670A95D0464B02024F3472(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA86CE6162DD0A7205EFBC5667C0C4DE20A878E6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_Reset_m4434BCF873055AC64E638CC68A9BE088A6E893C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_get_Current_mC421D7832D2EE3184491009EF400ECF57ECC89C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CamShake_tEE57505D5AE431F5278418747CF1D68AD15B504D_CustomAttributesCacheGenerator_CamShake_Shake_m8BA0D09CBBBF3CED787C61C31AB4EB65D845B1AA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_0_0_0_var), NULL);
	}
}
static void U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0__ctor_mE3930C98304840972F363AB1CB88536052692E8E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0_System_IDisposable_Dispose_m04F3B2696FD7252A2AA6387C6E5918C4A77CD6CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31F5CBCE42685E840CC48F69A43AF7B68EFD578A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0_System_Collections_IEnumerator_Reset_mDC5A7C99610E5FDC1BC6B42AD076A13127C82A06(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0_System_Collections_IEnumerator_get_Current_m28D18215B78D2FB82B6D302271A745C0B8DC7CB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CoffeePowerup_t333A40D1079B3D5BAE4EB78CDE4502B313A0630F_CustomAttributesCacheGenerator_CoffeePowerup_powerUpInitiated_m37AAB985B4AAE0080635766BC387C594938E485D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_0_0_0_var), NULL);
	}
}
static void U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8__ctor_mC8218FCEC77C1E08CF79FA5FB3746CC5750B791A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8_System_IDisposable_Dispose_mB5D5CAF6F02E76A474FCEDFB78A6762AF3DD4FF4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72330A707CB50F1297A7B0A3035EA6E951EEA7A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8_System_Collections_IEnumerator_Reset_m52DB922C34EE8FC9255DADE93FC88F20292EAF4D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8_System_Collections_IEnumerator_get_Current_m51F05BE80A89A005BD8F6DCED1C7948732B3D15F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CoffeeSpawner_t7116F5136B993DEE316ED9EF3E445DB254444DCF_CustomAttributesCacheGenerator_CoffeeSpawner_ifNotSpawned_m7A93C0A3792C8701C77F3D283D92449DFA4CF8CA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_0_0_0_var), NULL);
	}
}
static void U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6__ctor_m9D7C3351F4BCC06791FE71A599A713A721DCFC6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6_System_IDisposable_Dispose_mB1A0E50147F5C6BC9CB1CB919038DA86F6AB8382(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE76B3313972C13BE097AE649BF15E11371B47D8C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6_System_Collections_IEnumerator_Reset_m99D5C9748552F856BD2A9E17FD1694D6EF96A3ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6_System_Collections_IEnumerator_get_Current_m1CF2F16BA82BC904F79EC72098778A5226E84E52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EnemyAI_t145CB41D735873AEEB5EE9C216137FAC2BC8CA82_CustomAttributesCacheGenerator_ball(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ExitToMenu_t697069F2E10BE5ACB5F3771FC5A31F3A7AD39335_CustomAttributesCacheGenerator_ExitToMenu_ClickTime_m40DE6962C5D0B09DE47190ACC6B183DC0D90C530(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_0_0_0_var), NULL);
	}
}
static void U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3__ctor_m66EE8D269B45FFA6CCFF8FE3A68E6DD60F8755C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3_System_IDisposable_Dispose_m2EC353FF96FDF57C82A2A909A441582DAE50EA71(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D55AC17DC672E373E33BA879A208305B4078517(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3_System_Collections_IEnumerator_Reset_m59D4715CE65CCDF121C87FA30B042B5424444FA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3_System_Collections_IEnumerator_get_Current_m63BBDC3750711AAA1E338FD71CE99591580B2B7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SceneChanger_t6A30EA4853DA52DBD1479ADCBE7B3B6952D1E068_CustomAttributesCacheGenerator_SceneChanger_LoadLevel_mC6EE52E7168FCCBFF8F19F2F024B89214B6F1155(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_0_0_0_var), NULL);
	}
}
static void U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4__ctor_m98269A477E8C41845FB9869F15B455835BF93D9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4_System_IDisposable_Dispose_m1A23D1A82CBA38639A7F5E81D87C63E23853B694(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D67E20AA987A2E1D693795E61BFFC85C25E05CF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_Reset_m23744983BD3E0036F121559DB5106C4F91A0DD8E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C7D0378542A54483DAAA001588526A9942E703(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ExitGame_t5AD45A8DDD6F3A8DA0DD3D5210C5F2280D335353_CustomAttributesCacheGenerator_ExitGame_ClickTime_m93B1FB775916E19472F4DE4BB96063B2E9CE6C47(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_0_0_0_var), NULL);
	}
}
static void U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2__ctor_mBE48FB21CE0DDAAB65B9C8217837EAA56562D6D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2_System_IDisposable_Dispose_m246BB3532C53A2A57C50C910A37D6FAD9E2C7C84(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38C567F1D07014FF6070FF2D4CE1BF8E33CDEDF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2_System_Collections_IEnumerator_Reset_m059E259B9A94CAE1C8B62109F8A664024A322AEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2_System_Collections_IEnumerator_get_Current_m7ABBA01CCF480BEFFEE36AC88061A637690894C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SceneSwitcher_t0D5299E748616A52E7E6B47BC61FEB51CE91192F_CustomAttributesCacheGenerator_SceneSwitcher_LoadLevel_m76EB62B502044D73A5680325DEA8FC3D07FC3D6E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_0_0_0_var), NULL);
	}
}
static void SceneSwitcher_t0D5299E748616A52E7E6B47BC61FEB51CE91192F_CustomAttributesCacheGenerator_SceneSwitcher_LoadLevelwName_m6DDE937E8CD73F8374CF2212053173E1DAF6F0ED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_0_0_0_var), NULL);
	}
}
static void U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5__ctor_mB58A2F695E755860D780DE855A4454325901560F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5_System_IDisposable_Dispose_mB251295756043CE4CD24ACC65C575D95CE23DBB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA51ED35C9FC361A277C853F08603CF10AB7E3F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_Reset_m3586EC8B503E7496BC84ED60AACF6E8970FF5FF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_get_Current_m0F49277E0D112AA56E09D6A541CBAD9F1A9CD69C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6__ctor_m0B72C9DE54AE3653B98B3DDE23F788F1D39618EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6_System_IDisposable_Dispose_mA2C83A8393C887976144D0FB8E5E1FBFBDA4761E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m201BCB165709B514BB14A7FA5F9570395C250E7D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_Reset_mF0E537C43E8742B719320807B527E61706C16A10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_get_Current_m05539D487210A1253E1135E4BC2DF50B7EF10109(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x42\x61\x6C\x6C"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x42\x61\x6C\x6C\x73"), NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[78] = 
{
	U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator,
	U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator,
	U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator,
	U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator,
	U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator,
	U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator,
	U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator,
	U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator,
	U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator,
	U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator,
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_CustomAttributesCacheGenerator,
	CollisionControl_tD0D618A5A6C7164DC8D6A61123950D61038854EC_CustomAttributesCacheGenerator_movement,
	CollisionControl_tD0D618A5A6C7164DC8D6A61123950D61038854EC_CustomAttributesCacheGenerator_duration,
	CollisionControl_tD0D618A5A6C7164DC8D6A61123950D61038854EC_CustomAttributesCacheGenerator_effects,
	CollisionControl_tD0D618A5A6C7164DC8D6A61123950D61038854EC_CustomAttributesCacheGenerator_CoffeeSpawner,
	Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_speed,
	EnemyAI_t145CB41D735873AEEB5EE9C216137FAC2BC8CA82_CustomAttributesCacheGenerator_ball,
	Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_Movement_StartBall_m7AB43B4E9ED20C2A410DDC75B67F091DD784D0F8,
	Movement_t81B548083980F3B23F17AC9CA1C906E46F4B594C_CustomAttributesCacheGenerator_Movement_StartBallBegin_mC9FD9E1D18F5391F7DDDFA4F1E0FF4B2C76F3D7E,
	U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8__ctor_m97E2DD4D209082B6F7181E6D94703AA2500E3EB7,
	U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8_System_IDisposable_Dispose_mCEC7941AF2B3549148AF1A1179B92EFD13C5A921,
	U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A4EEACF2C3D6C0E6D266C22977FEE42495B5938,
	U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8_System_Collections_IEnumerator_Reset_mAE7E8A68110AFF3F722E7836993E410502A78AA8,
	U3CStartBallU3Ed__8_t0484C4D2B72A2987C31FC0A4E2C110BF79669782_CustomAttributesCacheGenerator_U3CStartBallU3Ed__8_System_Collections_IEnumerator_get_Current_m8683BEB4304E01DAC26D210346EF461C7D038388,
	U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9__ctor_m6426507D7E7C9AD7CCFD1882FD1452B43B1EB16B,
	U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9_System_IDisposable_Dispose_m573A70317648DC9FC8670A95D0464B02024F3472,
	U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA86CE6162DD0A7205EFBC5667C0C4DE20A878E6A,
	U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_Reset_m4434BCF873055AC64E638CC68A9BE088A6E893C7,
	U3CStartBallBeginU3Ed__9_t94F4512FF6E7B0365AE5B1FFF8BEF1CC605438CC_CustomAttributesCacheGenerator_U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_get_Current_mC421D7832D2EE3184491009EF400ECF57ECC89C6,
	CamShake_tEE57505D5AE431F5278418747CF1D68AD15B504D_CustomAttributesCacheGenerator_CamShake_Shake_m8BA0D09CBBBF3CED787C61C31AB4EB65D845B1AA,
	U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0__ctor_mE3930C98304840972F363AB1CB88536052692E8E,
	U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0_System_IDisposable_Dispose_m04F3B2696FD7252A2AA6387C6E5918C4A77CD6CA,
	U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31F5CBCE42685E840CC48F69A43AF7B68EFD578A,
	U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0_System_Collections_IEnumerator_Reset_mDC5A7C99610E5FDC1BC6B42AD076A13127C82A06,
	U3CShakeU3Ed__0_t24EBFAC4C4E90AF9A8A836C2DABD54E4732C2972_CustomAttributesCacheGenerator_U3CShakeU3Ed__0_System_Collections_IEnumerator_get_Current_m28D18215B78D2FB82B6D302271A745C0B8DC7CB8,
	CoffeePowerup_t333A40D1079B3D5BAE4EB78CDE4502B313A0630F_CustomAttributesCacheGenerator_CoffeePowerup_powerUpInitiated_m37AAB985B4AAE0080635766BC387C594938E485D,
	U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8__ctor_mC8218FCEC77C1E08CF79FA5FB3746CC5750B791A,
	U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8_System_IDisposable_Dispose_mB5D5CAF6F02E76A474FCEDFB78A6762AF3DD4FF4,
	U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72330A707CB50F1297A7B0A3035EA6E951EEA7A5,
	U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8_System_Collections_IEnumerator_Reset_m52DB922C34EE8FC9255DADE93FC88F20292EAF4D,
	U3CpowerUpInitiatedU3Ed__8_tF0E4B89E69D439948B341E536711D31E39453FA3_CustomAttributesCacheGenerator_U3CpowerUpInitiatedU3Ed__8_System_Collections_IEnumerator_get_Current_m51F05BE80A89A005BD8F6DCED1C7948732B3D15F,
	CoffeeSpawner_t7116F5136B993DEE316ED9EF3E445DB254444DCF_CustomAttributesCacheGenerator_CoffeeSpawner_ifNotSpawned_m7A93C0A3792C8701C77F3D283D92449DFA4CF8CA,
	U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6__ctor_m9D7C3351F4BCC06791FE71A599A713A721DCFC6A,
	U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6_System_IDisposable_Dispose_mB1A0E50147F5C6BC9CB1CB919038DA86F6AB8382,
	U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE76B3313972C13BE097AE649BF15E11371B47D8C,
	U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6_System_Collections_IEnumerator_Reset_m99D5C9748552F856BD2A9E17FD1694D6EF96A3ED,
	U3CifNotSpawnedU3Ed__6_t95969E09AF50B8858B7C841066082ED237B20DD3_CustomAttributesCacheGenerator_U3CifNotSpawnedU3Ed__6_System_Collections_IEnumerator_get_Current_m1CF2F16BA82BC904F79EC72098778A5226E84E52,
	ExitToMenu_t697069F2E10BE5ACB5F3771FC5A31F3A7AD39335_CustomAttributesCacheGenerator_ExitToMenu_ClickTime_m40DE6962C5D0B09DE47190ACC6B183DC0D90C530,
	U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3__ctor_m66EE8D269B45FFA6CCFF8FE3A68E6DD60F8755C0,
	U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3_System_IDisposable_Dispose_m2EC353FF96FDF57C82A2A909A441582DAE50EA71,
	U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D55AC17DC672E373E33BA879A208305B4078517,
	U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3_System_Collections_IEnumerator_Reset_m59D4715CE65CCDF121C87FA30B042B5424444FA7,
	U3CClickTimeU3Ed__3_tD543923803871908549AEA57D2084945EFC06AEC_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__3_System_Collections_IEnumerator_get_Current_m63BBDC3750711AAA1E338FD71CE99591580B2B7A,
	SceneChanger_t6A30EA4853DA52DBD1479ADCBE7B3B6952D1E068_CustomAttributesCacheGenerator_SceneChanger_LoadLevel_mC6EE52E7168FCCBFF8F19F2F024B89214B6F1155,
	U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4__ctor_m98269A477E8C41845FB9869F15B455835BF93D9D,
	U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4_System_IDisposable_Dispose_m1A23D1A82CBA38639A7F5E81D87C63E23853B694,
	U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D67E20AA987A2E1D693795E61BFFC85C25E05CF,
	U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_Reset_m23744983BD3E0036F121559DB5106C4F91A0DD8E,
	U3CLoadLevelU3Ed__4_t34FB881D2E9DCBC9A1E18D04685E046B29B53BF0_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C7D0378542A54483DAAA001588526A9942E703,
	ExitGame_t5AD45A8DDD6F3A8DA0DD3D5210C5F2280D335353_CustomAttributesCacheGenerator_ExitGame_ClickTime_m93B1FB775916E19472F4DE4BB96063B2E9CE6C47,
	U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2__ctor_mBE48FB21CE0DDAAB65B9C8217837EAA56562D6D5,
	U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2_System_IDisposable_Dispose_m246BB3532C53A2A57C50C910A37D6FAD9E2C7C84,
	U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38C567F1D07014FF6070FF2D4CE1BF8E33CDEDF3,
	U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2_System_Collections_IEnumerator_Reset_m059E259B9A94CAE1C8B62109F8A664024A322AEC,
	U3CClickTimeU3Ed__2_t6A5C68798536B11A0DD24EA8B229009E6A35CEF7_CustomAttributesCacheGenerator_U3CClickTimeU3Ed__2_System_Collections_IEnumerator_get_Current_m7ABBA01CCF480BEFFEE36AC88061A637690894C0,
	SceneSwitcher_t0D5299E748616A52E7E6B47BC61FEB51CE91192F_CustomAttributesCacheGenerator_SceneSwitcher_LoadLevel_m76EB62B502044D73A5680325DEA8FC3D07FC3D6E,
	SceneSwitcher_t0D5299E748616A52E7E6B47BC61FEB51CE91192F_CustomAttributesCacheGenerator_SceneSwitcher_LoadLevelwName_m6DDE937E8CD73F8374CF2212053173E1DAF6F0ED,
	U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5__ctor_mB58A2F695E755860D780DE855A4454325901560F,
	U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5_System_IDisposable_Dispose_mB251295756043CE4CD24ACC65C575D95CE23DBB8,
	U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA51ED35C9FC361A277C853F08603CF10AB7E3F1,
	U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_Reset_m3586EC8B503E7496BC84ED60AACF6E8970FF5FF8,
	U3CLoadLevelU3Ed__5_t942D517CD70F447001F30BB7C88D77133FF78DD7_CustomAttributesCacheGenerator_U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_get_Current_m0F49277E0D112AA56E09D6A541CBAD9F1A9CD69C,
	U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6__ctor_m0B72C9DE54AE3653B98B3DDE23F788F1D39618EA,
	U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6_System_IDisposable_Dispose_mA2C83A8393C887976144D0FB8E5E1FBFBDA4761E,
	U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m201BCB165709B514BB14A7FA5F9570395C250E7D,
	U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_Reset_mF0E537C43E8742B719320807B527E61706C16A10,
	U3CLoadLevelwNameU3Ed__6_tB030CAB8EC681DD399EBB612BE323B7B8FD73680_CustomAttributesCacheGenerator_U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_get_Current_m05539D487210A1253E1135E4BC2DF50B7EF10109,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
