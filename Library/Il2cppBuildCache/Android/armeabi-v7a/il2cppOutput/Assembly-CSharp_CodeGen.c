﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void SoundEffectsPlayer::ballHit()
extern void SoundEffectsPlayer_ballHit_m069EDA29C6C3B5A42CCB4515BC5531789062DE71 (void);
// 0x00000002 System.Void SoundEffectsPlayer::pointGainedSound()
extern void SoundEffectsPlayer_pointGainedSound_mCFD4C8737250BF8E8FB19872D3AF6D55C28A66BE (void);
// 0x00000003 System.Void SoundEffectsPlayer::.ctor()
extern void SoundEffectsPlayer__ctor_mCFBACA816F6E2F32A3DC640495341235FA9DE06C (void);
// 0x00000004 System.Void BallDisplay::Start()
extern void BallDisplay_Start_m215A760A6EC51E546A98DC886C91A6BD4A1BB8D5 (void);
// 0x00000005 System.Void BallDisplay::.ctor()
extern void BallDisplay__ctor_m27D36CDD7E3B5B2C7A9F86DA17F961C63B46744B (void);
// 0x00000006 System.Void CollisionControl::Start()
extern void CollisionControl_Start_m762B87AEBE94E078F113B53D6E041ABADCCFA6EE (void);
// 0x00000007 System.Void CollisionControl::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void CollisionControl_OnCollisionEnter2D_m5EBD8BA01F197237208A913FEBC1558AD4371A57 (void);
// 0x00000008 System.Void CollisionControl::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CollisionControl_OnTriggerEnter2D_m79968D29A948CF12F6EFD4DA948AB64A9B82F68F (void);
// 0x00000009 System.Void CollisionControl::.ctor()
extern void CollisionControl__ctor_mEDA714870AC54527C7433FB917CA299C8895B6C7 (void);
// 0x0000000A System.Void Movement::Start()
extern void Movement_Start_m5FA5146A031A9B13FE98F9CCD6027EB1DBA2DF4F (void);
// 0x0000000B System.Collections.IEnumerator Movement::StartBall()
extern void Movement_StartBall_m7AB43B4E9ED20C2A410DDC75B67F091DD784D0F8 (void);
// 0x0000000C System.Collections.IEnumerator Movement::StartBallBegin()
extern void Movement_StartBallBegin_mC9FD9E1D18F5391F7DDDFA4F1E0FF4B2C76F3D7E (void);
// 0x0000000D System.Void Movement::StartBallMovement()
extern void Movement_StartBallMovement_m748324942CAB3F88BB410B0DA69DE7D7F73CC452 (void);
// 0x0000000E System.Void Movement::MoveBall(UnityEngine.Vector2)
extern void Movement_MoveBall_m06255729831C9CA6C2C0A81C5679002C92372F83 (void);
// 0x0000000F System.Void Movement::Update()
extern void Movement_Update_m0880BACB69D5C89071A82EAB9BC17F76151B7DF1 (void);
// 0x00000010 System.Void Movement::IncreaseHitcount()
extern void Movement_IncreaseHitcount_mBD72F81A3B5A7CAF19D489D233D04B1C488A26CC (void);
// 0x00000011 System.Void Movement::BounceOfRacket(UnityEngine.Collision2D)
extern void Movement_BounceOfRacket_m3425C17B88E163F890C3C0CD2F8D581EC6396493 (void);
// 0x00000012 System.Void Movement::.ctor()
extern void Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB (void);
// 0x00000013 System.Void Movement::.cctor()
extern void Movement__cctor_mF0343773A9DACA99715A36BEACA9B41A0A54D2AB (void);
// 0x00000014 System.Void Movement/<StartBall>d__8::.ctor(System.Int32)
extern void U3CStartBallU3Ed__8__ctor_m97E2DD4D209082B6F7181E6D94703AA2500E3EB7 (void);
// 0x00000015 System.Void Movement/<StartBall>d__8::System.IDisposable.Dispose()
extern void U3CStartBallU3Ed__8_System_IDisposable_Dispose_mCEC7941AF2B3549148AF1A1179B92EFD13C5A921 (void);
// 0x00000016 System.Boolean Movement/<StartBall>d__8::MoveNext()
extern void U3CStartBallU3Ed__8_MoveNext_mF817E744F329249A20ED7E2C4578C584DD1258E9 (void);
// 0x00000017 System.Object Movement/<StartBall>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartBallU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A4EEACF2C3D6C0E6D266C22977FEE42495B5938 (void);
// 0x00000018 System.Void Movement/<StartBall>d__8::System.Collections.IEnumerator.Reset()
extern void U3CStartBallU3Ed__8_System_Collections_IEnumerator_Reset_mAE7E8A68110AFF3F722E7836993E410502A78AA8 (void);
// 0x00000019 System.Object Movement/<StartBall>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CStartBallU3Ed__8_System_Collections_IEnumerator_get_Current_m8683BEB4304E01DAC26D210346EF461C7D038388 (void);
// 0x0000001A System.Void Movement/<StartBallBegin>d__9::.ctor(System.Int32)
extern void U3CStartBallBeginU3Ed__9__ctor_m6426507D7E7C9AD7CCFD1882FD1452B43B1EB16B (void);
// 0x0000001B System.Void Movement/<StartBallBegin>d__9::System.IDisposable.Dispose()
extern void U3CStartBallBeginU3Ed__9_System_IDisposable_Dispose_m573A70317648DC9FC8670A95D0464B02024F3472 (void);
// 0x0000001C System.Boolean Movement/<StartBallBegin>d__9::MoveNext()
extern void U3CStartBallBeginU3Ed__9_MoveNext_mE8D58838EE777FC133BF1964AB3638C9CDC84873 (void);
// 0x0000001D System.Object Movement/<StartBallBegin>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartBallBeginU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA86CE6162DD0A7205EFBC5667C0C4DE20A878E6A (void);
// 0x0000001E System.Void Movement/<StartBallBegin>d__9::System.Collections.IEnumerator.Reset()
extern void U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_Reset_m4434BCF873055AC64E638CC68A9BE088A6E893C7 (void);
// 0x0000001F System.Object Movement/<StartBallBegin>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_get_Current_mC421D7832D2EE3184491009EF400ECF57ECC89C6 (void);
// 0x00000020 System.Void BackgroundTransition::Start()
extern void BackgroundTransition_Start_mBA2ED002AC45A14174B000D62302BF7C2A9EBAB9 (void);
// 0x00000021 System.Void BackgroundTransition::FixedUpdate()
extern void BackgroundTransition_FixedUpdate_mE7DD35ABBF4BBF37D2BCA5C63868C7640451DCE9 (void);
// 0x00000022 System.Void BackgroundTransition::SwitchBackground()
extern void BackgroundTransition_SwitchBackground_m05CC3682E46835820CA55537C9EA11DF8B517C42 (void);
// 0x00000023 System.Void BackgroundTransition::.ctor()
extern void BackgroundTransition__ctor_m2A5221C9342FF62D793B9C54D1F102166C720488 (void);
// 0x00000024 System.Void CamColliderSpawner::Start()
extern void CamColliderSpawner_Start_m7FADA39392209C0061D75D07BFBAFF31EFF4D8C3 (void);
// 0x00000025 System.Void CamColliderSpawner::.ctor()
extern void CamColliderSpawner__ctor_m8B04F73E1B071F4A920A547E76D10FA0B250D4AF (void);
// 0x00000026 System.Void CamEnemySpawner::SpawnOpponent()
extern void CamEnemySpawner_SpawnOpponent_m97815CD46CC7DDF97AD01085A50F7D67357FC2E4 (void);
// 0x00000027 System.Void CamEnemySpawner::.ctor()
extern void CamEnemySpawner__ctor_mD825D29495F2373C0CE93945473FE3675FFF112C (void);
// 0x00000028 System.Collections.IEnumerator CamShake::Shake(System.Single,System.Single)
extern void CamShake_Shake_m8BA0D09CBBBF3CED787C61C31AB4EB65D845B1AA (void);
// 0x00000029 System.Void CamShake::.ctor()
extern void CamShake__ctor_m58051E07E18C0D8CF1369900949A83AAF8D208B5 (void);
// 0x0000002A System.Void CamShake/<Shake>d__0::.ctor(System.Int32)
extern void U3CShakeU3Ed__0__ctor_mE3930C98304840972F363AB1CB88536052692E8E (void);
// 0x0000002B System.Void CamShake/<Shake>d__0::System.IDisposable.Dispose()
extern void U3CShakeU3Ed__0_System_IDisposable_Dispose_m04F3B2696FD7252A2AA6387C6E5918C4A77CD6CA (void);
// 0x0000002C System.Boolean CamShake/<Shake>d__0::MoveNext()
extern void U3CShakeU3Ed__0_MoveNext_mD9F8011C28EC7D85C72D5BB148730899826C8733 (void);
// 0x0000002D System.Object CamShake/<Shake>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShakeU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31F5CBCE42685E840CC48F69A43AF7B68EFD578A (void);
// 0x0000002E System.Void CamShake/<Shake>d__0::System.Collections.IEnumerator.Reset()
extern void U3CShakeU3Ed__0_System_Collections_IEnumerator_Reset_mDC5A7C99610E5FDC1BC6B42AD076A13127C82A06 (void);
// 0x0000002F System.Object CamShake/<Shake>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CShakeU3Ed__0_System_Collections_IEnumerator_get_Current_m28D18215B78D2FB82B6D302271A745C0B8DC7CB8 (void);
// 0x00000030 System.Void CamTransition::Awake()
extern void CamTransition_Awake_mFDC2BABF0E3AEB8CAA99F938F5CF9A9AFAD514DA (void);
// 0x00000031 System.Void CamTransition::FixedUpdate()
extern void CamTransition_FixedUpdate_m4C6B447FB29B250C720AE64857F0C39309B01034 (void);
// 0x00000032 System.Void CamTransition::MoveCam()
extern void CamTransition_MoveCam_mEDD6C5BA3E877687A882844A4F3DED4F90F59CC3 (void);
// 0x00000033 System.Void CamTransition::.ctor()
extern void CamTransition__ctor_mA1BA6F36C957FE341F167722E76D4A3FF86B8412 (void);
// 0x00000034 System.Void CoffeePickup::Coffee()
extern void CoffeePickup_Coffee_mE9A405E4581F9EA46112CF7A053A63F5ADF1294A (void);
// 0x00000035 System.Void CoffeePickup::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CoffeePickup_OnTriggerEnter2D_mAFD574B7C557688F09A5874D8FD64F057C798319 (void);
// 0x00000036 System.Void CoffeePickup::.ctor()
extern void CoffeePickup__ctor_mE08CC5A9F3650B106D5C26578C58DA5D0ED75F37 (void);
// 0x00000037 System.Void CoffeePowerup::Start()
extern void CoffeePowerup_Start_m1EE981F3F6D8B5D6C5D6B43E8C48D24253C262B6 (void);
// 0x00000038 System.Void CoffeePowerup::Powerup()
extern void CoffeePowerup_Powerup_mB960F9CFCB520F51AE1ABF8C6A493C9B4D650874 (void);
// 0x00000039 System.Collections.IEnumerator CoffeePowerup::powerUpInitiated()
extern void CoffeePowerup_powerUpInitiated_m37AAB985B4AAE0080635766BC387C594938E485D (void);
// 0x0000003A System.Void CoffeePowerup::.ctor()
extern void CoffeePowerup__ctor_mE0FBBDCA3FC2F78CDF7E97EF3F866A93372EE4C0 (void);
// 0x0000003B System.Void CoffeePowerup/<powerUpInitiated>d__8::.ctor(System.Int32)
extern void U3CpowerUpInitiatedU3Ed__8__ctor_mC8218FCEC77C1E08CF79FA5FB3746CC5750B791A (void);
// 0x0000003C System.Void CoffeePowerup/<powerUpInitiated>d__8::System.IDisposable.Dispose()
extern void U3CpowerUpInitiatedU3Ed__8_System_IDisposable_Dispose_mB5D5CAF6F02E76A474FCEDFB78A6762AF3DD4FF4 (void);
// 0x0000003D System.Boolean CoffeePowerup/<powerUpInitiated>d__8::MoveNext()
extern void U3CpowerUpInitiatedU3Ed__8_MoveNext_m0E52265FC846D9A3419921883840FB09BC79505B (void);
// 0x0000003E System.Object CoffeePowerup/<powerUpInitiated>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CpowerUpInitiatedU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72330A707CB50F1297A7B0A3035EA6E951EEA7A5 (void);
// 0x0000003F System.Void CoffeePowerup/<powerUpInitiated>d__8::System.Collections.IEnumerator.Reset()
extern void U3CpowerUpInitiatedU3Ed__8_System_Collections_IEnumerator_Reset_m52DB922C34EE8FC9255DADE93FC88F20292EAF4D (void);
// 0x00000040 System.Object CoffeePowerup/<powerUpInitiated>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CpowerUpInitiatedU3Ed__8_System_Collections_IEnumerator_get_Current_m51F05BE80A89A005BD8F6DCED1C7948732B3D15F (void);
// 0x00000041 System.Void CoffeeSpawner::SpawnCoffee()
extern void CoffeeSpawner_SpawnCoffee_m3439AFA717ED0F65638D2C341C70CBCC40480798 (void);
// 0x00000042 System.Collections.IEnumerator CoffeeSpawner::ifNotSpawned()
extern void CoffeeSpawner_ifNotSpawned_m7A93C0A3792C8701C77F3D283D92449DFA4CF8CA (void);
// 0x00000043 System.Void CoffeeSpawner::.ctor()
extern void CoffeeSpawner__ctor_mBA3E7D4B2FED12F603A1FB4FB069567EF1F7B796 (void);
// 0x00000044 System.Void CoffeeSpawner/<ifNotSpawned>d__6::.ctor(System.Int32)
extern void U3CifNotSpawnedU3Ed__6__ctor_m9D7C3351F4BCC06791FE71A599A713A721DCFC6A (void);
// 0x00000045 System.Void CoffeeSpawner/<ifNotSpawned>d__6::System.IDisposable.Dispose()
extern void U3CifNotSpawnedU3Ed__6_System_IDisposable_Dispose_mB1A0E50147F5C6BC9CB1CB919038DA86F6AB8382 (void);
// 0x00000046 System.Boolean CoffeeSpawner/<ifNotSpawned>d__6::MoveNext()
extern void U3CifNotSpawnedU3Ed__6_MoveNext_m4151AA035983FC4643748A9097731FD552B04814 (void);
// 0x00000047 System.Object CoffeeSpawner/<ifNotSpawned>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CifNotSpawnedU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE76B3313972C13BE097AE649BF15E11371B47D8C (void);
// 0x00000048 System.Void CoffeeSpawner/<ifNotSpawned>d__6::System.Collections.IEnumerator.Reset()
extern void U3CifNotSpawnedU3Ed__6_System_Collections_IEnumerator_Reset_m99D5C9748552F856BD2A9E17FD1694D6EF96A3ED (void);
// 0x00000049 System.Object CoffeeSpawner/<ifNotSpawned>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CifNotSpawnedU3Ed__6_System_Collections_IEnumerator_get_Current_m1CF2F16BA82BC904F79EC72098778A5226E84E52 (void);
// 0x0000004A System.Void EnemyAI::Start()
extern void EnemyAI_Start_mC5494C97D0214D04302BAE4F82F48D13AD0D25BE (void);
// 0x0000004B System.Void EnemyAI::Update()
extern void EnemyAI_Update_mBB23B2D10604094D0D5BDD6D02A5C51DFCDFAA84 (void);
// 0x0000004C System.Void EnemyAI::Confine()
extern void EnemyAI_Confine_m9DBF0F03A4959391B7356488FA43E6E77E6BFD99 (void);
// 0x0000004D System.Void EnemyAI::.ctor()
extern void EnemyAI__ctor_mAF4FEC29EA4ADF864B4641448BFF55028ED4B3BC (void);
// 0x0000004E System.Void BallAssist::ScoredToLoad()
extern void BallAssist_ScoredToLoad_mC518CC038C39319C9E89A30E39361805231F8F18 (void);
// 0x0000004F System.Void BallAssist::DifficultyIncrease()
extern void BallAssist_DifficultyIncrease_mC31F8E8115CE363876C87D9A2F92373709961DCF (void);
// 0x00000050 System.Void BallAssist::.ctor()
extern void BallAssist__ctor_m440CDEDE6467806934B88EA9B61427FA2AA3FB0E (void);
// 0x00000051 System.Void ExitToMenu::FixedUpdate()
extern void ExitToMenu_FixedUpdate_m25A162E87CF7B0549CC656876FDFAB667A5B5BF5 (void);
// 0x00000052 System.Collections.IEnumerator ExitToMenu::ClickTime()
extern void ExitToMenu_ClickTime_m40DE6962C5D0B09DE47190ACC6B183DC0D90C530 (void);
// 0x00000053 System.Void ExitToMenu::GoToMenu()
extern void ExitToMenu_GoToMenu_m2B54B32F88ADC7497652B11DC986F7651DAFD372 (void);
// 0x00000054 System.Void ExitToMenu::.ctor()
extern void ExitToMenu__ctor_m5FF120BD5E0BB095CC2F2066885C992AD6BD1CFC (void);
// 0x00000055 System.Void ExitToMenu/<ClickTime>d__3::.ctor(System.Int32)
extern void U3CClickTimeU3Ed__3__ctor_m66EE8D269B45FFA6CCFF8FE3A68E6DD60F8755C0 (void);
// 0x00000056 System.Void ExitToMenu/<ClickTime>d__3::System.IDisposable.Dispose()
extern void U3CClickTimeU3Ed__3_System_IDisposable_Dispose_m2EC353FF96FDF57C82A2A909A441582DAE50EA71 (void);
// 0x00000057 System.Boolean ExitToMenu/<ClickTime>d__3::MoveNext()
extern void U3CClickTimeU3Ed__3_MoveNext_mB79C62F52105730FE9C730F3F373EF3918499868 (void);
// 0x00000058 System.Object ExitToMenu/<ClickTime>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClickTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D55AC17DC672E373E33BA879A208305B4078517 (void);
// 0x00000059 System.Void ExitToMenu/<ClickTime>d__3::System.Collections.IEnumerator.Reset()
extern void U3CClickTimeU3Ed__3_System_Collections_IEnumerator_Reset_m59D4715CE65CCDF121C87FA30B042B5424444FA7 (void);
// 0x0000005A System.Object ExitToMenu/<ClickTime>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CClickTimeU3Ed__3_System_Collections_IEnumerator_get_Current_m63BBDC3750711AAA1E338FD71CE99591580B2B7A (void);
// 0x0000005B System.Void GameOver::Start()
extern void GameOver_Start_m55E09F90FA409F47F1A42929F294DC60C06FD5CB (void);
// 0x0000005C System.Void GameOver::PlayGameOver()
extern void GameOver_PlayGameOver_m45378CFF43DD4F2BD107FECFE66C9271BC6FFC96 (void);
// 0x0000005D System.Void GameOver::Continue()
extern void GameOver_Continue_mD6C8EA01B891A701AAE4FE9B765E550F8D2EE493 (void);
// 0x0000005E System.Void GameOver::.ctor()
extern void GameOver__ctor_mA5E40A40961F188E4CE253D481E8AED893A224E9 (void);
// 0x0000005F System.Void LifeSystem::Awake()
extern void LifeSystem_Awake_m7E5BBEE508A0CF8A73E8FD2D180AA1C6785E6C29 (void);
// 0x00000060 System.Void LifeSystem::Start()
extern void LifeSystem_Start_m97C00C2A716B05465A579C2184EAC7AC626BE2C1 (void);
// 0x00000061 System.Void LifeSystem::Update()
extern void LifeSystem_Update_m6DFD85F8BA692C166246AF7D62BEE016FC5E5F1E (void);
// 0x00000062 System.Void LifeSystem::UseLife()
extern void LifeSystem_UseLife_m981A36983F44C969AE24578BA2C2559490D6887F (void);
// 0x00000063 System.Void LifeSystem::ClaimLives()
extern void LifeSystem_ClaimLives_m2E5AAAB0C3FDBBD747A69D7A5E08947ECA5A8073 (void);
// 0x00000064 System.Void LifeSystem::.ctor()
extern void LifeSystem__ctor_m09023F1EC2DB6D321F505DD282E0788033378427 (void);
// 0x00000065 System.Void PauseMenu::Awake()
extern void PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C (void);
// 0x00000066 System.Void PauseMenu::Resume()
extern void PauseMenu_Resume_m256AFDD68DF9851B6D716189F709ED93D45C3717 (void);
// 0x00000067 System.Void PauseMenu::Pause()
extern void PauseMenu_Pause_m395165A04A026E9974327328181ACFA512DD76C7 (void);
// 0x00000068 System.Void PauseMenu::GameOverResume()
extern void PauseMenu_GameOverResume_mEDE4C4716B6C17FFD51593F8B980834B44AAF2D4 (void);
// 0x00000069 System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018 (void);
// 0x0000006A System.Void PauseMenu::.cctor()
extern void PauseMenu__cctor_m6819B2CD3B03CB44F1D7914C847058A7C1B7D924 (void);
// 0x0000006B System.Void QualityManager::Start()
extern void QualityManager_Start_mDDFD56B05EC1DE4359D4B89C287414DF89764A2D (void);
// 0x0000006C System.Void QualityManager::SetHighQuality()
extern void QualityManager_SetHighQuality_mB7362089A2FBCE5CCD4E159EC373D88B182E8CB2 (void);
// 0x0000006D System.Void QualityManager::SetLowQuality()
extern void QualityManager_SetLowQuality_m9F5D14705EC64F7B771E8E4A2D61C1A4FEF11F4A (void);
// 0x0000006E System.Void QualityManager::MusicOn()
extern void QualityManager_MusicOn_mFF5B0C204E85FACDFF77195DAE82E1EBB79E1FB6 (void);
// 0x0000006F System.Void QualityManager::MusicOff()
extern void QualityManager_MusicOff_mA669F433A0A1EB92BFA5D46479856C4FD50EE454 (void);
// 0x00000070 System.Void QualityManager::CheckQuality()
extern void QualityManager_CheckQuality_mF6BCEEF134822C86812948D4D9D4C2907668C323 (void);
// 0x00000071 System.Void QualityManager::.ctor()
extern void QualityManager__ctor_m4D2A1B00D815FF12918378E4F470B9C91006750C (void);
// 0x00000072 System.Void QualityManager::.cctor()
extern void QualityManager__cctor_mEB6FA22B83CE06400BD28CD4E8470A06678C99E2 (void);
// 0x00000073 System.Void SavingData::Start()
extern void SavingData_Start_m44EA2572CED579D7A09EC9EECA751B95F10BF34F (void);
// 0x00000074 System.Void SavingData::FetchData()
extern void SavingData_FetchData_m4F3DB7E8EC0D099963D3A29795EB0D029800B224 (void);
// 0x00000075 System.Void SavingData::SaveData()
extern void SavingData_SaveData_m39F00DDB6ADE3AFBE74C78298AD8C36F3B8494E8 (void);
// 0x00000076 System.Void SavingData::.ctor()
extern void SavingData__ctor_m369D131945FA5FB272B0894AEC17532FB7303D0C (void);
// 0x00000077 System.Void SceneChanger::LoadMenu()
extern void SceneChanger_LoadMenu_mB9EF2A09D51B781E8C210D063CF57A8A80065B34 (void);
// 0x00000078 System.Collections.IEnumerator SceneChanger::LoadLevel(System.Int32)
extern void SceneChanger_LoadLevel_mC6EE52E7168FCCBFF8F19F2F024B89214B6F1155 (void);
// 0x00000079 System.Void SceneChanger::.ctor()
extern void SceneChanger__ctor_m11AE9A596EFE92EE1AA22BD7A48AB0C1D758AB1D (void);
// 0x0000007A System.Void SceneChanger/<LoadLevel>d__4::.ctor(System.Int32)
extern void U3CLoadLevelU3Ed__4__ctor_m98269A477E8C41845FB9869F15B455835BF93D9D (void);
// 0x0000007B System.Void SceneChanger/<LoadLevel>d__4::System.IDisposable.Dispose()
extern void U3CLoadLevelU3Ed__4_System_IDisposable_Dispose_m1A23D1A82CBA38639A7F5E81D87C63E23853B694 (void);
// 0x0000007C System.Boolean SceneChanger/<LoadLevel>d__4::MoveNext()
extern void U3CLoadLevelU3Ed__4_MoveNext_m0B5BFA879ADB04B102DA8EB6C627B6918326DA67 (void);
// 0x0000007D System.Object SceneChanger/<LoadLevel>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLevelU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D67E20AA987A2E1D693795E61BFFC85C25E05CF (void);
// 0x0000007E System.Void SceneChanger/<LoadLevel>d__4::System.Collections.IEnumerator.Reset()
extern void U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_Reset_m23744983BD3E0036F121559DB5106C4F91A0DD8E (void);
// 0x0000007F System.Object SceneChanger/<LoadLevel>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C7D0378542A54483DAAA001588526A9942E703 (void);
// 0x00000080 System.Void ScoreController::Start()
extern void ScoreController_Start_m0FD1B3E5F0084EBF933F55927DA715760ADFB1E0 (void);
// 0x00000081 System.Void ScoreController::FixedUpdate()
extern void ScoreController_FixedUpdate_mC0B37BED3A9353F55A8392585D043DBE2BA56C65 (void);
// 0x00000082 System.Void ScoreController::Scored()
extern void ScoreController_Scored_mFDAC3705CB1978503B29D8B3E3365B6E99C3BA6F (void);
// 0x00000083 System.Void ScoreController::.ctor()
extern void ScoreController__ctor_m1C404EA3B84DC168F4ACDDB65E7425FF01FA5DFD (void);
// 0x00000084 System.Void StatsManagement::FixedUpdate()
extern void StatsManagement_FixedUpdate_mA055E8E6A91584A18E90AF9F77C1371A3DF0E61B (void);
// 0x00000085 System.Void StatsManagement::.ctor()
extern void StatsManagement__ctor_mBC06FF59CE51053633CE7A51D345B8C5031A8EC8 (void);
// 0x00000086 UnityEngine.GameObject EnemyFinder::FindClosestEnemy()
extern void EnemyFinder_FindClosestEnemy_m6FAF28FE89643CE51B82C16A87F34EB8C46F7DDA (void);
// 0x00000087 System.Void EnemyFinder::EnemyDed()
extern void EnemyFinder_EnemyDed_m4E371C2BD074CC92A9B97DC858030B2D99757A2B (void);
// 0x00000088 System.Void EnemyFinder::.ctor()
extern void EnemyFinder__ctor_m649FA95220B62F158D07DA0CF6612BD096A6A609 (void);
// 0x00000089 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x0000008A System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x0000008B System.Void PlayerController::.cctor()
extern void PlayerController__cctor_m85342AF0280476580815CF892EC4D72DAC42C5CA (void);
// 0x0000008C System.Void PlayerData::.ctor(SavingData)
extern void PlayerData__ctor_m7B1549BDC8953091C97FC752106DAAFB225FBD4F (void);
// 0x0000008D System.Void SaveSystem::SaveData(SavingData)
extern void SaveSystem_SaveData_m79798DEC622E7F4D7A305264FE6A34E8B0A43D83 (void);
// 0x0000008E PlayerData SaveSystem::LoadData()
extern void SaveSystem_LoadData_mF18CC53A0D4AF72DF3EBEB114C9D09E3195A8DFF (void);
// 0x0000008F System.Void BuyWithAds::PlayAds()
extern void BuyWithAds_PlayAds_mE08CF7981DC166629503DE631FC7F636882F8609 (void);
// 0x00000090 System.Void BuyWithAds::EndAds()
extern void BuyWithAds_EndAds_m3BE98F8CB7AE11F61597F12052DD23C08C227ADB (void);
// 0x00000091 System.Void BuyWithAds::.ctor()
extern void BuyWithAds__ctor_m38C4F19B9759E6C4C39333C01DB9D22D634092C5 (void);
// 0x00000092 System.Void SettingsAnimControl::MenuTrigger()
extern void SettingsAnimControl_MenuTrigger_mBE44867E35BE7929B0D04BA592B6356900A81D52 (void);
// 0x00000093 System.Void SettingsAnimControl::OpenMenu()
extern void SettingsAnimControl_OpenMenu_mCD5467013502E3CD17510F8062FC62921FC75F00 (void);
// 0x00000094 System.Void SettingsAnimControl::CloseMenu()
extern void SettingsAnimControl_CloseMenu_m503F2B6D632FE99E07D6E07608604B0BB6D1305F (void);
// 0x00000095 System.Void SettingsAnimControl::.ctor()
extern void SettingsAnimControl__ctor_m60898838273F2B75602E924E880CDECE9204333E (void);
// 0x00000096 System.Void BuyLives::BuyALife(System.Int32)
extern void BuyLives_BuyALife_m6A1802D18A0FE89A3F568306EACB48DA2CE7D370 (void);
// 0x00000097 System.Void BuyLives::BuyALifeWithAds()
extern void BuyLives_BuyALifeWithAds_m509FDC8311658D79084741365D61831D9568633F (void);
// 0x00000098 System.Void BuyLives::OpenLivesPanel()
extern void BuyLives_OpenLivesPanel_m01F5986A9CA617BF18A64FCD9751CDBF7FAF4D76 (void);
// 0x00000099 System.Void BuyLives::.ctor()
extern void BuyLives__ctor_mC7F76C8FB11AC98CACCD163CA2478092D4AB27B8 (void);
// 0x0000009A System.Void LifeTimer::Start()
extern void LifeTimer_Start_m16A682144DB5A7A0384EC1817965D93B1DCEDC88 (void);
// 0x0000009B System.Void LifeTimer::Update()
extern void LifeTimer_Update_mA0C98082C4D3328F32F763ACADDD9E3AFCFF4055 (void);
// 0x0000009C System.Void LifeTimer::LifeClick()
extern void LifeTimer_LifeClick_mBB1C551959250F93087217607E7C30EA883F96A0 (void);
// 0x0000009D System.Boolean LifeTimer::isLifeReady()
extern void LifeTimer_isLifeReady_m0A1243800B62F7DA0A0618B3A72F071BC131855F (void);
// 0x0000009E System.Void LifeTimer::.ctor()
extern void LifeTimer__ctor_m41E2EAAB50DD5496ADC104280D3C0AECDB513F45 (void);
// 0x0000009F System.Void ExitGame::FixedUpdate()
extern void ExitGame_FixedUpdate_m0A7291F0F613D2A87B1EB8C5B07FEEE8F2A06EDB (void);
// 0x000000A0 System.Collections.IEnumerator ExitGame::ClickTime()
extern void ExitGame_ClickTime_m93B1FB775916E19472F4DE4BB96063B2E9CE6C47 (void);
// 0x000000A1 System.Void ExitGame::.ctor()
extern void ExitGame__ctor_mE36C6D0891DAC163174132A765057A45F82E96FA (void);
// 0x000000A2 System.Void ExitGame/<ClickTime>d__2::.ctor(System.Int32)
extern void U3CClickTimeU3Ed__2__ctor_mBE48FB21CE0DDAAB65B9C8217837EAA56562D6D5 (void);
// 0x000000A3 System.Void ExitGame/<ClickTime>d__2::System.IDisposable.Dispose()
extern void U3CClickTimeU3Ed__2_System_IDisposable_Dispose_m246BB3532C53A2A57C50C910A37D6FAD9E2C7C84 (void);
// 0x000000A4 System.Boolean ExitGame/<ClickTime>d__2::MoveNext()
extern void U3CClickTimeU3Ed__2_MoveNext_mCC9199E760DF6D44F6761EE64A34A11845FDB4B1 (void);
// 0x000000A5 System.Object ExitGame/<ClickTime>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClickTimeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38C567F1D07014FF6070FF2D4CE1BF8E33CDEDF3 (void);
// 0x000000A6 System.Void ExitGame/<ClickTime>d__2::System.Collections.IEnumerator.Reset()
extern void U3CClickTimeU3Ed__2_System_Collections_IEnumerator_Reset_m059E259B9A94CAE1C8B62109F8A664024A322AEC (void);
// 0x000000A7 System.Object ExitGame/<ClickTime>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CClickTimeU3Ed__2_System_Collections_IEnumerator_get_Current_m7ABBA01CCF480BEFFEE36AC88061A637690894C0 (void);
// 0x000000A8 System.Void HighScoreManager::Start()
extern void HighScoreManager_Start_mDF8FC039129AB4EF0DC95B24981558B78C6239D2 (void);
// 0x000000A9 System.Void HighScoreManager::LoadData()
extern void HighScoreManager_LoadData_mA79535092B0CC9E7AF5361622691F0861A6EC788 (void);
// 0x000000AA System.Void HighScoreManager::Reset()
extern void HighScoreManager_Reset_m2ACF29190D608752A1A69EC8A307A41018B30133 (void);
// 0x000000AB System.Void HighScoreManager::.ctor()
extern void HighScoreManager__ctor_m3EB965F94EEA5E0853803B557808F8DB0FAD6EF6 (void);
// 0x000000AC System.Void SceneSwitcher::SwitchLeft()
extern void SceneSwitcher_SwitchLeft_m2DEF0C090022210518FB005E14F17318FA4F59F5 (void);
// 0x000000AD System.Void SceneSwitcher::SwitchRight()
extern void SceneSwitcher_SwitchRight_mBF6F05BF11B82766FCBB9F7163582F51200C28D3 (void);
// 0x000000AE System.Void SceneSwitcher::PlayGame(System.String)
extern void SceneSwitcher_PlayGame_mF346E8A9A73BA7F556F609BD120F104C834A615F (void);
// 0x000000AF System.Collections.IEnumerator SceneSwitcher::LoadLevel(System.Int32)
extern void SceneSwitcher_LoadLevel_m76EB62B502044D73A5680325DEA8FC3D07FC3D6E (void);
// 0x000000B0 System.Collections.IEnumerator SceneSwitcher::LoadLevelwName(System.String)
extern void SceneSwitcher_LoadLevelwName_m6DDE937E8CD73F8374CF2212053173E1DAF6F0ED (void);
// 0x000000B1 System.Void SceneSwitcher::.ctor()
extern void SceneSwitcher__ctor_mBB4CC9B16BCD8440C5F9641D2F4ABEFC99C213AC (void);
// 0x000000B2 System.Void SceneSwitcher/<LoadLevel>d__5::.ctor(System.Int32)
extern void U3CLoadLevelU3Ed__5__ctor_mB58A2F695E755860D780DE855A4454325901560F (void);
// 0x000000B3 System.Void SceneSwitcher/<LoadLevel>d__5::System.IDisposable.Dispose()
extern void U3CLoadLevelU3Ed__5_System_IDisposable_Dispose_mB251295756043CE4CD24ACC65C575D95CE23DBB8 (void);
// 0x000000B4 System.Boolean SceneSwitcher/<LoadLevel>d__5::MoveNext()
extern void U3CLoadLevelU3Ed__5_MoveNext_mD617CF35E62BD21018DDC2BA0B54B0ABEE44B14A (void);
// 0x000000B5 System.Object SceneSwitcher/<LoadLevel>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLevelU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA51ED35C9FC361A277C853F08603CF10AB7E3F1 (void);
// 0x000000B6 System.Void SceneSwitcher/<LoadLevel>d__5::System.Collections.IEnumerator.Reset()
extern void U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_Reset_m3586EC8B503E7496BC84ED60AACF6E8970FF5FF8 (void);
// 0x000000B7 System.Object SceneSwitcher/<LoadLevel>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_get_Current_m0F49277E0D112AA56E09D6A541CBAD9F1A9CD69C (void);
// 0x000000B8 System.Void SceneSwitcher/<LoadLevelwName>d__6::.ctor(System.Int32)
extern void U3CLoadLevelwNameU3Ed__6__ctor_m0B72C9DE54AE3653B98B3DDE23F788F1D39618EA (void);
// 0x000000B9 System.Void SceneSwitcher/<LoadLevelwName>d__6::System.IDisposable.Dispose()
extern void U3CLoadLevelwNameU3Ed__6_System_IDisposable_Dispose_mA2C83A8393C887976144D0FB8E5E1FBFBDA4761E (void);
// 0x000000BA System.Boolean SceneSwitcher/<LoadLevelwName>d__6::MoveNext()
extern void U3CLoadLevelwNameU3Ed__6_MoveNext_mC25CFC47D2EEF910357CF900C8757992C3F24B32 (void);
// 0x000000BB System.Object SceneSwitcher/<LoadLevelwName>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLevelwNameU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m201BCB165709B514BB14A7FA5F9570395C250E7D (void);
// 0x000000BC System.Void SceneSwitcher/<LoadLevelwName>d__6::System.Collections.IEnumerator.Reset()
extern void U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_Reset_mF0E537C43E8742B719320807B527E61706C16A10 (void);
// 0x000000BD System.Object SceneSwitcher/<LoadLevelwName>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_get_Current_m05539D487210A1253E1135E4BC2DF50B7EF10109 (void);
// 0x000000BE System.Void ThemeManager::Start()
extern void ThemeManager_Start_mE8E3EB2A3DCAA7475A2853733A0FD604FFBC8319 (void);
// 0x000000BF System.Void ThemeManager::ThemeSwitch(System.Boolean)
extern void ThemeManager_ThemeSwitch_m6B039AAB70A73F2834A8021CE01A87C787112FF5 (void);
// 0x000000C0 System.Void ThemeManager::.ctor()
extern void ThemeManager__ctor_m35ED54BF10F8BC0CED14E65C33154C7A69A9BD55 (void);
// 0x000000C1 System.Void Resetter::Start()
extern void Resetter_Start_m2E2F20065205338C64F18CED7ED414EDDFFD2294 (void);
// 0x000000C2 System.Void Resetter::.ctor()
extern void Resetter__ctor_m3B139EED787F48FFCECFC5B868B86C78AD0820E5 (void);
// 0x000000C3 System.Void Resetter::.cctor()
extern void Resetter__cctor_m5BFF7F3C9B6041A5CA485B675A874F728EC61C36 (void);
// 0x000000C4 System.Void Ball::.ctor()
extern void Ball__ctor_m76D6811A7C300B95F347A08A5731B731A60E1D25 (void);
// 0x000000C5 System.Void StartMainMenu::GoToMainMenu()
extern void StartMainMenu_GoToMainMenu_m8AEAC143C261A0D7DD66F139EFCF847DFABB12A5 (void);
// 0x000000C6 System.Void StartMainMenu::.ctor()
extern void StartMainMenu__ctor_m652BB01B84FA9B0CB6FA4A4FCFD425E85799D2F4 (void);
static Il2CppMethodPointer s_methodPointers[198] = 
{
	SoundEffectsPlayer_ballHit_m069EDA29C6C3B5A42CCB4515BC5531789062DE71,
	SoundEffectsPlayer_pointGainedSound_mCFD4C8737250BF8E8FB19872D3AF6D55C28A66BE,
	SoundEffectsPlayer__ctor_mCFBACA816F6E2F32A3DC640495341235FA9DE06C,
	BallDisplay_Start_m215A760A6EC51E546A98DC886C91A6BD4A1BB8D5,
	BallDisplay__ctor_m27D36CDD7E3B5B2C7A9F86DA17F961C63B46744B,
	CollisionControl_Start_m762B87AEBE94E078F113B53D6E041ABADCCFA6EE,
	CollisionControl_OnCollisionEnter2D_m5EBD8BA01F197237208A913FEBC1558AD4371A57,
	CollisionControl_OnTriggerEnter2D_m79968D29A948CF12F6EFD4DA948AB64A9B82F68F,
	CollisionControl__ctor_mEDA714870AC54527C7433FB917CA299C8895B6C7,
	Movement_Start_m5FA5146A031A9B13FE98F9CCD6027EB1DBA2DF4F,
	Movement_StartBall_m7AB43B4E9ED20C2A410DDC75B67F091DD784D0F8,
	Movement_StartBallBegin_mC9FD9E1D18F5391F7DDDFA4F1E0FF4B2C76F3D7E,
	Movement_StartBallMovement_m748324942CAB3F88BB410B0DA69DE7D7F73CC452,
	Movement_MoveBall_m06255729831C9CA6C2C0A81C5679002C92372F83,
	Movement_Update_m0880BACB69D5C89071A82EAB9BC17F76151B7DF1,
	Movement_IncreaseHitcount_mBD72F81A3B5A7CAF19D489D233D04B1C488A26CC,
	Movement_BounceOfRacket_m3425C17B88E163F890C3C0CD2F8D581EC6396493,
	Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB,
	Movement__cctor_mF0343773A9DACA99715A36BEACA9B41A0A54D2AB,
	U3CStartBallU3Ed__8__ctor_m97E2DD4D209082B6F7181E6D94703AA2500E3EB7,
	U3CStartBallU3Ed__8_System_IDisposable_Dispose_mCEC7941AF2B3549148AF1A1179B92EFD13C5A921,
	U3CStartBallU3Ed__8_MoveNext_mF817E744F329249A20ED7E2C4578C584DD1258E9,
	U3CStartBallU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6A4EEACF2C3D6C0E6D266C22977FEE42495B5938,
	U3CStartBallU3Ed__8_System_Collections_IEnumerator_Reset_mAE7E8A68110AFF3F722E7836993E410502A78AA8,
	U3CStartBallU3Ed__8_System_Collections_IEnumerator_get_Current_m8683BEB4304E01DAC26D210346EF461C7D038388,
	U3CStartBallBeginU3Ed__9__ctor_m6426507D7E7C9AD7CCFD1882FD1452B43B1EB16B,
	U3CStartBallBeginU3Ed__9_System_IDisposable_Dispose_m573A70317648DC9FC8670A95D0464B02024F3472,
	U3CStartBallBeginU3Ed__9_MoveNext_mE8D58838EE777FC133BF1964AB3638C9CDC84873,
	U3CStartBallBeginU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA86CE6162DD0A7205EFBC5667C0C4DE20A878E6A,
	U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_Reset_m4434BCF873055AC64E638CC68A9BE088A6E893C7,
	U3CStartBallBeginU3Ed__9_System_Collections_IEnumerator_get_Current_mC421D7832D2EE3184491009EF400ECF57ECC89C6,
	BackgroundTransition_Start_mBA2ED002AC45A14174B000D62302BF7C2A9EBAB9,
	BackgroundTransition_FixedUpdate_mE7DD35ABBF4BBF37D2BCA5C63868C7640451DCE9,
	BackgroundTransition_SwitchBackground_m05CC3682E46835820CA55537C9EA11DF8B517C42,
	BackgroundTransition__ctor_m2A5221C9342FF62D793B9C54D1F102166C720488,
	CamColliderSpawner_Start_m7FADA39392209C0061D75D07BFBAFF31EFF4D8C3,
	CamColliderSpawner__ctor_m8B04F73E1B071F4A920A547E76D10FA0B250D4AF,
	CamEnemySpawner_SpawnOpponent_m97815CD46CC7DDF97AD01085A50F7D67357FC2E4,
	CamEnemySpawner__ctor_mD825D29495F2373C0CE93945473FE3675FFF112C,
	CamShake_Shake_m8BA0D09CBBBF3CED787C61C31AB4EB65D845B1AA,
	CamShake__ctor_m58051E07E18C0D8CF1369900949A83AAF8D208B5,
	U3CShakeU3Ed__0__ctor_mE3930C98304840972F363AB1CB88536052692E8E,
	U3CShakeU3Ed__0_System_IDisposable_Dispose_m04F3B2696FD7252A2AA6387C6E5918C4A77CD6CA,
	U3CShakeU3Ed__0_MoveNext_mD9F8011C28EC7D85C72D5BB148730899826C8733,
	U3CShakeU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31F5CBCE42685E840CC48F69A43AF7B68EFD578A,
	U3CShakeU3Ed__0_System_Collections_IEnumerator_Reset_mDC5A7C99610E5FDC1BC6B42AD076A13127C82A06,
	U3CShakeU3Ed__0_System_Collections_IEnumerator_get_Current_m28D18215B78D2FB82B6D302271A745C0B8DC7CB8,
	CamTransition_Awake_mFDC2BABF0E3AEB8CAA99F938F5CF9A9AFAD514DA,
	CamTransition_FixedUpdate_m4C6B447FB29B250C720AE64857F0C39309B01034,
	CamTransition_MoveCam_mEDD6C5BA3E877687A882844A4F3DED4F90F59CC3,
	CamTransition__ctor_mA1BA6F36C957FE341F167722E76D4A3FF86B8412,
	CoffeePickup_Coffee_mE9A405E4581F9EA46112CF7A053A63F5ADF1294A,
	CoffeePickup_OnTriggerEnter2D_mAFD574B7C557688F09A5874D8FD64F057C798319,
	CoffeePickup__ctor_mE08CC5A9F3650B106D5C26578C58DA5D0ED75F37,
	CoffeePowerup_Start_m1EE981F3F6D8B5D6C5D6B43E8C48D24253C262B6,
	CoffeePowerup_Powerup_mB960F9CFCB520F51AE1ABF8C6A493C9B4D650874,
	CoffeePowerup_powerUpInitiated_m37AAB985B4AAE0080635766BC387C594938E485D,
	CoffeePowerup__ctor_mE0FBBDCA3FC2F78CDF7E97EF3F866A93372EE4C0,
	U3CpowerUpInitiatedU3Ed__8__ctor_mC8218FCEC77C1E08CF79FA5FB3746CC5750B791A,
	U3CpowerUpInitiatedU3Ed__8_System_IDisposable_Dispose_mB5D5CAF6F02E76A474FCEDFB78A6762AF3DD4FF4,
	U3CpowerUpInitiatedU3Ed__8_MoveNext_m0E52265FC846D9A3419921883840FB09BC79505B,
	U3CpowerUpInitiatedU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m72330A707CB50F1297A7B0A3035EA6E951EEA7A5,
	U3CpowerUpInitiatedU3Ed__8_System_Collections_IEnumerator_Reset_m52DB922C34EE8FC9255DADE93FC88F20292EAF4D,
	U3CpowerUpInitiatedU3Ed__8_System_Collections_IEnumerator_get_Current_m51F05BE80A89A005BD8F6DCED1C7948732B3D15F,
	CoffeeSpawner_SpawnCoffee_m3439AFA717ED0F65638D2C341C70CBCC40480798,
	CoffeeSpawner_ifNotSpawned_m7A93C0A3792C8701C77F3D283D92449DFA4CF8CA,
	CoffeeSpawner__ctor_mBA3E7D4B2FED12F603A1FB4FB069567EF1F7B796,
	U3CifNotSpawnedU3Ed__6__ctor_m9D7C3351F4BCC06791FE71A599A713A721DCFC6A,
	U3CifNotSpawnedU3Ed__6_System_IDisposable_Dispose_mB1A0E50147F5C6BC9CB1CB919038DA86F6AB8382,
	U3CifNotSpawnedU3Ed__6_MoveNext_m4151AA035983FC4643748A9097731FD552B04814,
	U3CifNotSpawnedU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE76B3313972C13BE097AE649BF15E11371B47D8C,
	U3CifNotSpawnedU3Ed__6_System_Collections_IEnumerator_Reset_m99D5C9748552F856BD2A9E17FD1694D6EF96A3ED,
	U3CifNotSpawnedU3Ed__6_System_Collections_IEnumerator_get_Current_m1CF2F16BA82BC904F79EC72098778A5226E84E52,
	EnemyAI_Start_mC5494C97D0214D04302BAE4F82F48D13AD0D25BE,
	EnemyAI_Update_mBB23B2D10604094D0D5BDD6D02A5C51DFCDFAA84,
	EnemyAI_Confine_m9DBF0F03A4959391B7356488FA43E6E77E6BFD99,
	EnemyAI__ctor_mAF4FEC29EA4ADF864B4641448BFF55028ED4B3BC,
	BallAssist_ScoredToLoad_mC518CC038C39319C9E89A30E39361805231F8F18,
	BallAssist_DifficultyIncrease_mC31F8E8115CE363876C87D9A2F92373709961DCF,
	BallAssist__ctor_m440CDEDE6467806934B88EA9B61427FA2AA3FB0E,
	ExitToMenu_FixedUpdate_m25A162E87CF7B0549CC656876FDFAB667A5B5BF5,
	ExitToMenu_ClickTime_m40DE6962C5D0B09DE47190ACC6B183DC0D90C530,
	ExitToMenu_GoToMenu_m2B54B32F88ADC7497652B11DC986F7651DAFD372,
	ExitToMenu__ctor_m5FF120BD5E0BB095CC2F2066885C992AD6BD1CFC,
	U3CClickTimeU3Ed__3__ctor_m66EE8D269B45FFA6CCFF8FE3A68E6DD60F8755C0,
	U3CClickTimeU3Ed__3_System_IDisposable_Dispose_m2EC353FF96FDF57C82A2A909A441582DAE50EA71,
	U3CClickTimeU3Ed__3_MoveNext_mB79C62F52105730FE9C730F3F373EF3918499868,
	U3CClickTimeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D55AC17DC672E373E33BA879A208305B4078517,
	U3CClickTimeU3Ed__3_System_Collections_IEnumerator_Reset_m59D4715CE65CCDF121C87FA30B042B5424444FA7,
	U3CClickTimeU3Ed__3_System_Collections_IEnumerator_get_Current_m63BBDC3750711AAA1E338FD71CE99591580B2B7A,
	GameOver_Start_m55E09F90FA409F47F1A42929F294DC60C06FD5CB,
	GameOver_PlayGameOver_m45378CFF43DD4F2BD107FECFE66C9271BC6FFC96,
	GameOver_Continue_mD6C8EA01B891A701AAE4FE9B765E550F8D2EE493,
	GameOver__ctor_mA5E40A40961F188E4CE253D481E8AED893A224E9,
	LifeSystem_Awake_m7E5BBEE508A0CF8A73E8FD2D180AA1C6785E6C29,
	LifeSystem_Start_m97C00C2A716B05465A579C2184EAC7AC626BE2C1,
	LifeSystem_Update_m6DFD85F8BA692C166246AF7D62BEE016FC5E5F1E,
	LifeSystem_UseLife_m981A36983F44C969AE24578BA2C2559490D6887F,
	LifeSystem_ClaimLives_m2E5AAAB0C3FDBBD747A69D7A5E08947ECA5A8073,
	LifeSystem__ctor_m09023F1EC2DB6D321F505DD282E0788033378427,
	PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C,
	PauseMenu_Resume_m256AFDD68DF9851B6D716189F709ED93D45C3717,
	PauseMenu_Pause_m395165A04A026E9974327328181ACFA512DD76C7,
	PauseMenu_GameOverResume_mEDE4C4716B6C17FFD51593F8B980834B44AAF2D4,
	PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018,
	PauseMenu__cctor_m6819B2CD3B03CB44F1D7914C847058A7C1B7D924,
	QualityManager_Start_mDDFD56B05EC1DE4359D4B89C287414DF89764A2D,
	QualityManager_SetHighQuality_mB7362089A2FBCE5CCD4E159EC373D88B182E8CB2,
	QualityManager_SetLowQuality_m9F5D14705EC64F7B771E8E4A2D61C1A4FEF11F4A,
	QualityManager_MusicOn_mFF5B0C204E85FACDFF77195DAE82E1EBB79E1FB6,
	QualityManager_MusicOff_mA669F433A0A1EB92BFA5D46479856C4FD50EE454,
	QualityManager_CheckQuality_mF6BCEEF134822C86812948D4D9D4C2907668C323,
	QualityManager__ctor_m4D2A1B00D815FF12918378E4F470B9C91006750C,
	QualityManager__cctor_mEB6FA22B83CE06400BD28CD4E8470A06678C99E2,
	SavingData_Start_m44EA2572CED579D7A09EC9EECA751B95F10BF34F,
	SavingData_FetchData_m4F3DB7E8EC0D099963D3A29795EB0D029800B224,
	SavingData_SaveData_m39F00DDB6ADE3AFBE74C78298AD8C36F3B8494E8,
	SavingData__ctor_m369D131945FA5FB272B0894AEC17532FB7303D0C,
	SceneChanger_LoadMenu_mB9EF2A09D51B781E8C210D063CF57A8A80065B34,
	SceneChanger_LoadLevel_mC6EE52E7168FCCBFF8F19F2F024B89214B6F1155,
	SceneChanger__ctor_m11AE9A596EFE92EE1AA22BD7A48AB0C1D758AB1D,
	U3CLoadLevelU3Ed__4__ctor_m98269A477E8C41845FB9869F15B455835BF93D9D,
	U3CLoadLevelU3Ed__4_System_IDisposable_Dispose_m1A23D1A82CBA38639A7F5E81D87C63E23853B694,
	U3CLoadLevelU3Ed__4_MoveNext_m0B5BFA879ADB04B102DA8EB6C627B6918326DA67,
	U3CLoadLevelU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D67E20AA987A2E1D693795E61BFFC85C25E05CF,
	U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_Reset_m23744983BD3E0036F121559DB5106C4F91A0DD8E,
	U3CLoadLevelU3Ed__4_System_Collections_IEnumerator_get_Current_mB1C7D0378542A54483DAAA001588526A9942E703,
	ScoreController_Start_m0FD1B3E5F0084EBF933F55927DA715760ADFB1E0,
	ScoreController_FixedUpdate_mC0B37BED3A9353F55A8392585D043DBE2BA56C65,
	ScoreController_Scored_mFDAC3705CB1978503B29D8B3E3365B6E99C3BA6F,
	ScoreController__ctor_m1C404EA3B84DC168F4ACDDB65E7425FF01FA5DFD,
	StatsManagement_FixedUpdate_mA055E8E6A91584A18E90AF9F77C1371A3DF0E61B,
	StatsManagement__ctor_mBC06FF59CE51053633CE7A51D345B8C5031A8EC8,
	EnemyFinder_FindClosestEnemy_m6FAF28FE89643CE51B82C16A87F34EB8C46F7DDA,
	EnemyFinder_EnemyDed_m4E371C2BD074CC92A9B97DC858030B2D99757A2B,
	EnemyFinder__ctor_m649FA95220B62F158D07DA0CF6612BD096A6A609,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	PlayerController__cctor_m85342AF0280476580815CF892EC4D72DAC42C5CA,
	PlayerData__ctor_m7B1549BDC8953091C97FC752106DAAFB225FBD4F,
	SaveSystem_SaveData_m79798DEC622E7F4D7A305264FE6A34E8B0A43D83,
	SaveSystem_LoadData_mF18CC53A0D4AF72DF3EBEB114C9D09E3195A8DFF,
	BuyWithAds_PlayAds_mE08CF7981DC166629503DE631FC7F636882F8609,
	BuyWithAds_EndAds_m3BE98F8CB7AE11F61597F12052DD23C08C227ADB,
	BuyWithAds__ctor_m38C4F19B9759E6C4C39333C01DB9D22D634092C5,
	SettingsAnimControl_MenuTrigger_mBE44867E35BE7929B0D04BA592B6356900A81D52,
	SettingsAnimControl_OpenMenu_mCD5467013502E3CD17510F8062FC62921FC75F00,
	SettingsAnimControl_CloseMenu_m503F2B6D632FE99E07D6E07608604B0BB6D1305F,
	SettingsAnimControl__ctor_m60898838273F2B75602E924E880CDECE9204333E,
	BuyLives_BuyALife_m6A1802D18A0FE89A3F568306EACB48DA2CE7D370,
	BuyLives_BuyALifeWithAds_m509FDC8311658D79084741365D61831D9568633F,
	BuyLives_OpenLivesPanel_m01F5986A9CA617BF18A64FCD9751CDBF7FAF4D76,
	BuyLives__ctor_mC7F76C8FB11AC98CACCD163CA2478092D4AB27B8,
	LifeTimer_Start_m16A682144DB5A7A0384EC1817965D93B1DCEDC88,
	LifeTimer_Update_mA0C98082C4D3328F32F763ACADDD9E3AFCFF4055,
	LifeTimer_LifeClick_mBB1C551959250F93087217607E7C30EA883F96A0,
	LifeTimer_isLifeReady_m0A1243800B62F7DA0A0618B3A72F071BC131855F,
	LifeTimer__ctor_m41E2EAAB50DD5496ADC104280D3C0AECDB513F45,
	ExitGame_FixedUpdate_m0A7291F0F613D2A87B1EB8C5B07FEEE8F2A06EDB,
	ExitGame_ClickTime_m93B1FB775916E19472F4DE4BB96063B2E9CE6C47,
	ExitGame__ctor_mE36C6D0891DAC163174132A765057A45F82E96FA,
	U3CClickTimeU3Ed__2__ctor_mBE48FB21CE0DDAAB65B9C8217837EAA56562D6D5,
	U3CClickTimeU3Ed__2_System_IDisposable_Dispose_m246BB3532C53A2A57C50C910A37D6FAD9E2C7C84,
	U3CClickTimeU3Ed__2_MoveNext_mCC9199E760DF6D44F6761EE64A34A11845FDB4B1,
	U3CClickTimeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m38C567F1D07014FF6070FF2D4CE1BF8E33CDEDF3,
	U3CClickTimeU3Ed__2_System_Collections_IEnumerator_Reset_m059E259B9A94CAE1C8B62109F8A664024A322AEC,
	U3CClickTimeU3Ed__2_System_Collections_IEnumerator_get_Current_m7ABBA01CCF480BEFFEE36AC88061A637690894C0,
	HighScoreManager_Start_mDF8FC039129AB4EF0DC95B24981558B78C6239D2,
	HighScoreManager_LoadData_mA79535092B0CC9E7AF5361622691F0861A6EC788,
	HighScoreManager_Reset_m2ACF29190D608752A1A69EC8A307A41018B30133,
	HighScoreManager__ctor_m3EB965F94EEA5E0853803B557808F8DB0FAD6EF6,
	SceneSwitcher_SwitchLeft_m2DEF0C090022210518FB005E14F17318FA4F59F5,
	SceneSwitcher_SwitchRight_mBF6F05BF11B82766FCBB9F7163582F51200C28D3,
	SceneSwitcher_PlayGame_mF346E8A9A73BA7F556F609BD120F104C834A615F,
	SceneSwitcher_LoadLevel_m76EB62B502044D73A5680325DEA8FC3D07FC3D6E,
	SceneSwitcher_LoadLevelwName_m6DDE937E8CD73F8374CF2212053173E1DAF6F0ED,
	SceneSwitcher__ctor_mBB4CC9B16BCD8440C5F9641D2F4ABEFC99C213AC,
	U3CLoadLevelU3Ed__5__ctor_mB58A2F695E755860D780DE855A4454325901560F,
	U3CLoadLevelU3Ed__5_System_IDisposable_Dispose_mB251295756043CE4CD24ACC65C575D95CE23DBB8,
	U3CLoadLevelU3Ed__5_MoveNext_mD617CF35E62BD21018DDC2BA0B54B0ABEE44B14A,
	U3CLoadLevelU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA51ED35C9FC361A277C853F08603CF10AB7E3F1,
	U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_Reset_m3586EC8B503E7496BC84ED60AACF6E8970FF5FF8,
	U3CLoadLevelU3Ed__5_System_Collections_IEnumerator_get_Current_m0F49277E0D112AA56E09D6A541CBAD9F1A9CD69C,
	U3CLoadLevelwNameU3Ed__6__ctor_m0B72C9DE54AE3653B98B3DDE23F788F1D39618EA,
	U3CLoadLevelwNameU3Ed__6_System_IDisposable_Dispose_mA2C83A8393C887976144D0FB8E5E1FBFBDA4761E,
	U3CLoadLevelwNameU3Ed__6_MoveNext_mC25CFC47D2EEF910357CF900C8757992C3F24B32,
	U3CLoadLevelwNameU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m201BCB165709B514BB14A7FA5F9570395C250E7D,
	U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_Reset_mF0E537C43E8742B719320807B527E61706C16A10,
	U3CLoadLevelwNameU3Ed__6_System_Collections_IEnumerator_get_Current_m05539D487210A1253E1135E4BC2DF50B7EF10109,
	ThemeManager_Start_mE8E3EB2A3DCAA7475A2853733A0FD604FFBC8319,
	ThemeManager_ThemeSwitch_m6B039AAB70A73F2834A8021CE01A87C787112FF5,
	ThemeManager__ctor_m35ED54BF10F8BC0CED14E65C33154C7A69A9BD55,
	Resetter_Start_m2E2F20065205338C64F18CED7ED414EDDFFD2294,
	Resetter__ctor_m3B139EED787F48FFCECFC5B868B86C78AD0820E5,
	Resetter__cctor_m5BFF7F3C9B6041A5CA485B675A874F728EC61C36,
	Ball__ctor_m76D6811A7C300B95F347A08A5731B731A60E1D25,
	StartMainMenu_GoToMainMenu_m8AEAC143C261A0D7DD66F139EFCF847DFABB12A5,
	StartMainMenu__ctor_m652BB01B84FA9B0CB6FA4A4FCFD425E85799D2F4,
};
static const int32_t s_InvokerIndices[198] = 
{
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1564,
	1564,
	1931,
	1931,
	1874,
	1874,
	1931,
	1610,
	1931,
	1931,
	1564,
	1931,
	3030,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	637,
	1931,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1931,
	1931,
	1931,
	1931,
	1931,
	1564,
	1931,
	1931,
	1931,
	1874,
	1931,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1931,
	1874,
	1931,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1874,
	1931,
	1931,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	3030,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	3030,
	1931,
	1931,
	1931,
	1931,
	1931,
	1176,
	1931,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1874,
	1931,
	1931,
	1931,
	1931,
	3030,
	1564,
	2986,
	3012,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1552,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1899,
	1931,
	1931,
	1874,
	1931,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1931,
	1931,
	1931,
	1931,
	1931,
	1931,
	1564,
	1176,
	1180,
	1931,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1552,
	1931,
	1899,
	1874,
	1931,
	1874,
	1931,
	1584,
	1931,
	1931,
	1931,
	3030,
	1931,
	1931,
	1931,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	198,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
