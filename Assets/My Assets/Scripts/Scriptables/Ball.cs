﻿/*
* Copyright (c) ElementalDestructor
*/
//using UnityEditor.UIElements;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ball", menuName = "Balls")]
public class Ball : ScriptableObject
{
	public new string name;
	public string description;
	public string tag = "BallGraphic";

	public Sprite sprite;

	public int ID;
}
