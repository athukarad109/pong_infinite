﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class BuyWithAds : MonoBehaviour
{
	#region Initialization

	public Animator animator;
	//public AdsManagerUnity adsManager;

	#endregion

	#region Function

	public void PlayAds()
	{
		animator.SetTrigger("Start");
		//adsManager.ShowAd();
	}

	public void EndAds()
	{
		animator.SetTrigger("End");
	}

	#endregion
}
