﻿/*
* Copyright (c) ElementalDestructor
*/
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
	#region Initialization

	public Animator transition;
	public float transitiontime = 1f;

	#endregion

	#region Switcher

	public void SwitchLeft()
	{
		StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex - 1));
	}

	public void SwitchRight()
	{
		StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + 1));
	}

	public void PlayGame(string name)
	{
		Debug.Log("BOOP");
		StartCoroutine(LoadLevelwName(name));
	}

    #endregion

	#region Transition

	IEnumerator LoadLevel(int levelindex)
	{
		transition.SetTrigger("Start");

		yield return new WaitForSeconds(transitiontime);

		SceneManager.LoadScene(levelindex);
	}

	IEnumerator LoadLevelwName(string levelname)
	{
		transition.SetTrigger("Start");

		yield return new WaitForSeconds(transitiontime);

		SceneManager.LoadScene(levelname);
	}

	#endregion
}
