﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;
using TMPro;

public class HighScoreManager : MonoBehaviour
{
    public bool reset = false;
    public GameObject HighScoreText;
    public GameObject HighScoreText_UD;

    private void Start()
    {
        LoadData();
    }

    public void LoadData()
    {
        if (reset)
        {
            Reset();
        }

        TextMeshProUGUI PlayerHighScore = HighScoreText.GetComponent<TextMeshProUGUI>();
        PlayerHighScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();

        TextMeshProUGUI Underlaying = HighScoreText_UD.GetComponent<TextMeshProUGUI>();
        Underlaying.text = PlayerPrefs.GetInt("HighScore", 0).ToString();

        int LBScore = PlayerPrefs.GetInt("HighScore", 0);
        PlayerPrefs.SetInt("ScoreUpload", LBScore);
    }

    private void Reset()
    {
        PlayerPrefs.SetInt("HighScore", 0);
        PlayerPrefs.SetInt("Coffee", 0);
    }
}
