﻿/*
* Copyright (c) ElementalDestructor
*/
using System.Collections;
using UnityEngine;

public class ExitGame : MonoBehaviour
{
	#region Function

	int click;

	private void FixedUpdate()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			click++;
			StartCoroutine(ClickTime());

			if (click > 1)
				Application.Quit();
		}
	}

	IEnumerator ClickTime()
	{
		yield return new WaitForSeconds(0.5f);
		click = 0;
	}

	#endregion
}
