﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class ThemeManager : MonoBehaviour
{
	#region Initialization

	public GameObject[] Themes;
	int index;

	private void Start()
	{
		Themes = new GameObject[transform.childCount];

		for (int i = 0; i < transform.childCount; i++)
			Themes[i] = transform.GetChild(i).gameObject;

		foreach(GameObject g in Themes)
		{
			g.SetActive(false);
		}

		if (Themes[0])
			Themes[0].SetActive(true);
	}

	#endregion

	#region Function

	public void ThemeSwitch(bool isRight)
	{
		if (isRight)
		{
			Themes[index].SetActive(false);

			index--;

			if (index < 0)
				index = Themes.Length - 1;

			Themes[index].SetActive(true);
		}

		if (!isRight)
		{
			Themes[index].SetActive(false);

			index--;

			if (index < 0)
				index = Themes.Length - 1;

			Themes[index].SetActive(true);
		}
	}

	#endregion
}
