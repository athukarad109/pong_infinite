﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class SettingsAnimControl : MonoBehaviour
{
	#region Initialization

	public Animator animator;
	bool isOpen = false;

	#endregion

	#region Function

	public void MenuTrigger()
	{
		Debug.Log("Clicked");
		isOpen = !isOpen;

		if (isOpen)
			OpenMenu();
		if (!isOpen)
			CloseMenu();
	}

	private void OpenMenu()
	{
		animator.Play("Start");
	}

	private void CloseMenu()
	{
		animator.Play("End");
	}

	#endregion
}
