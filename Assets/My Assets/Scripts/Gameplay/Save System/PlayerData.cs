﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int Score;
    public int coffeecup;

    public PlayerData(SavingData savingData)
    {
        Score = savingData.Score;

        if (PlayerPrefs.GetInt("HighScore") < Score)
        {
            PlayerPrefs.SetInt("HighScore", Score);
        }

        coffeecup = savingData.coffeecups;
        int Coffee = PlayerPrefs.GetInt("Coffee", 0) + coffeecup;
        PlayerPrefs.SetInt("Coffee", Coffee);
    }
}
