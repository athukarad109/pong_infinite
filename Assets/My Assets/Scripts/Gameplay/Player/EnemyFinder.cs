﻿/*
* Copyright (c) ElementalDestructor
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFinder : MonoBehaviour
{
    #region  Function

    GameObject closest;
    float distance;
    Vector3 position;
    public GameObject enemyDeadEffect;

    public GameObject FindClosestEnemy()
    {
        GameObject[] opponents;
        opponents = GameObject.FindGameObjectsWithTag("Enemy");
        closest = null;
        distance = Mathf.Infinity;
        position = transform.position;

        foreach (GameObject opponent in opponents)
        {
            Vector3 diff = opponent.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = opponent;
                distance = curDistance;
            }
        }

        EnemyDed();

        Destroy(closest.transform.parent.gameObject);
        return (closest);
    }
	#endregion

	#region Enemy Dead Effect Spawner

    void EnemyDed()
	{
        GameObject g = Instantiate(enemyDeadEffect);
        g.transform.position = closest.transform.position;
        Destroy(g, 0.5f);
	}

	#endregion
}
