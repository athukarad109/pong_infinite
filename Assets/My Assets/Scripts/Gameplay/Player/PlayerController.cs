﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    bool moveAllowed;
    public float multiplier;
    public float screendivide;
    public static bool isPlayerActive = true;
    public static bool startgame = false;
    public static bool gameStarted = false;

    void Update()
    {
        if (isPlayerActive == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (Input.mousePosition.y < Screen.height / 2 - screendivide)
                {
                    moveAllowed = true;
                    if(!gameStarted)
					{
                        startgame = true;
                        gameStarted = true;
                    }
                }
            }


            if (moveAllowed)
            {
                Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector3 newpos = new Vector3(cursorPos.x, transform.position.y, transform.position.z);
                transform.position = Vector2.Lerp(transform.position, newpos, Time.deltaTime * multiplier);
            }

            if (Input.GetMouseButtonUp(0))
                moveAllowed = false;
        }
        else
        {
            return;
        }
    }
}
