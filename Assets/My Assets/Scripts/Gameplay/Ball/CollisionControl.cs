﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class CollisionControl : MonoBehaviour
{
    #region Initialization

    [Header ("Function Info :")]
    Movement movement;
    BallAssist ballAssist;
    SavingData saving;
    GameOver gameOver;
    CamShake CamShake;
    public GameObject particlePrefab;

    [Header("Camera Shake :")]
    public float duration;
    public float magnitude;

    [Header("Audio Effects")]
    SoundEffectsPlayer effects;


	private void Start()
	{
        movement = gameObject.GetComponent<Movement>();
        ballAssist = GameObject.FindGameObjectWithTag("Manager").GetComponent<BallAssist>();
        saving = GameObject.FindGameObjectWithTag("Manager").GetComponent<SavingData>();
        gameOver = GameObject.FindGameObjectWithTag("Manager").GetComponent<GameOver>();
        CamShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CamShake>();
        effects = FindObjectOfType<SoundEffectsPlayer>();
	}

	#endregion

	#region Collision Control

	// Collision detection
	private void OnCollisionEnter2D(Collision2D collision)
    {
		if (collision.gameObject.tag == "Player")
		{
            Quaternion rot = Quaternion.identity;
            rot.eulerAngles = new Vector3(0, 0, 0);
            GameObject g = Instantiate(particlePrefab, transform.position, rot);
            Destroy(g, 1f);
		}
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Enemy" )
        {
            movement.BounceOfRacket(collision);
            effects.ballHit();
        }
        else if (collision.gameObject.tag == "Right Wall" || collision.gameObject.tag == "Left Wall")
        {
            StartCoroutine(CamShake.Shake(duration, magnitude));
        }
        if (collision.gameObject.tag == "Right Wall")
		{
            Quaternion rot = Quaternion.identity;
            rot.eulerAngles = new Vector3(0, 0, 90);
            GameObject g = Instantiate(particlePrefab, transform.position , rot);
            Destroy(g, 1f);
        }
        if (collision.gameObject.tag == "Left Wall")
		{
            Quaternion rot = Quaternion.identity;
            rot.eulerAngles = new Vector3(0, 0, -90);
            GameObject g = Instantiate(particlePrefab, transform.position, rot);
            Destroy(g, 1f);
        }
        if (collision.gameObject.tag == "Enemy")
        {
            Quaternion rot = Quaternion.identity;
            rot.eulerAngles = new Vector3(0, 0, 180);
            GameObject g = Instantiate(particlePrefab, transform.position, rot);
            Destroy(g, 1f);
        }
    }

    // Scoring and destroying the trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Top Wall")
        {
            ballAssist.ScoredToLoad();

            Destroy(collision.gameObject);

            effects.pointGainedSound();
        }

        if (collision.gameObject.tag == "Bottom Wall")
        {
            saving.SaveData();

            gameOver.PlayGameOver();

            gameObject.SetActive(false);
        }

        if(collision.gameObject.tag == "Coffee")
		{
            int coffee = PlayerPrefs.GetInt("Coffee", 0);
            PlayerPrefs.SetInt("Coffee", coffee + 1);
		}
    }

    #endregion
}
