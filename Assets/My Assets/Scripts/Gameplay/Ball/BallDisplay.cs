﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;
using UnityEngine.UI;

public class BallDisplay : MonoBehaviour
{
	public Ball[] balls;
	private new SpriteRenderer renderer;
	private Image image;
	public bool isImage = false;
	public int ballId;

	private void Start()
	{
		//ballId = PlayerPrefs.GetInt("SelectedItemID", 0);
		if (isImage)
		{
			foreach (Ball ball in balls)
			{
				if (ballId == ball.ID)
				{
					image = gameObject.GetComponent<Image>();
					image.sprite = ball.sprite;
				}
			}
		}
		if (!isImage)
		{
			foreach (Ball ball in balls)
			{
				if (ballId == ball.ID)
				{
					renderer = gameObject.GetComponent<SpriteRenderer>();
					renderer.sprite = ball.sprite;
				}
			}
		}
		
	}
}
