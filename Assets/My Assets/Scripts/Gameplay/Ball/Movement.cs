﻿/*
* Copyright (c) ElementalDestructor
*/
using System.Collections;
using UnityEngine;

public class Movement : MonoBehaviour
{
    #region Initialization

    [SerializeField]
    public float speed;
    public float movementSpeed = 10f;
    public float speedMultiplayer = 2f;
    public float maxSpeed;
    Rigidbody2D rb;
    public int hitcount = 0;
    public static bool canBounce = true;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    #endregion

    #region Ball Movement

    public IEnumerator StartBall()
    {
        yield return new WaitForSeconds(2);
        MoveBall(new Vector2(0f, -1f));
    }

    public IEnumerator StartBallBegin()
    {
        yield return new WaitForSeconds(0.2f);
        MoveBall(new Vector2(0f, -1f));
    }

    public void StartBallMovement()
    {
        StartCoroutine(StartBall());
    }

    public void MoveBall(Vector2 direction)
    {
        if (direction.magnitude > 0)
        {
            direction.Normalize();

            speed = movementSpeed + hitcount * speedMultiplayer;

            rb.velocity = direction * speed * Time.fixedDeltaTime * 100;
        }
    }

	private void Update()
	{
		if (PlayerController.startgame)
		{
            StartCoroutine(StartBallBegin());
            PlayerController.startgame = false;
		}
	}

	public void IncreaseHitcount()
    {
        if (speed <= maxSpeed)
        {
            hitcount++;
        }
    }

    // Bouncing Mechanics
    public void BounceOfRacket(Collision2D collision2D)
    {
		if (canBounce)
		{
            Vector3 ballPosition = transform.position;
            Vector3 racketPosition = collision2D.transform.position;

            float y;
            if (collision2D.gameObject.name == "Player")
            {
                y = 1;
            }
            else
            {
                y = -1;
            }

            float x = (ballPosition.x - racketPosition.x);

            IncreaseHitcount();
            MoveBall(new Vector2(x, y));
        }
    }

    #endregion
}
