﻿/*
* Copyright (c) ElementalDestructor
*/
using System.Collections;
using UnityEngine;

public class ExitToMenu : MonoBehaviour
{
	#region Function

	int click;
	SceneChanger sceneChanger;

	private void FixedUpdate()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			click++;
			StartCoroutine(ClickTime());

			if (click > 1)
			{
				GoToMenu();
			}
				
		}
	}

	IEnumerator ClickTime()
	{
		yield return new WaitForSeconds(0.5f);
		click = 0;
	}

	void GoToMenu()
	{
		sceneChanger = gameObject.GetComponent<SceneChanger>();
		sceneChanger.LoadMenu();
	}

	#endregion
}
