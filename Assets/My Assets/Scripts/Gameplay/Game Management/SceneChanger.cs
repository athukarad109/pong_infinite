﻿/*
* Copyright (c) ElementalDestructor
*/
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
	#region Initialization

	public Animator transition;
	public float transitiontime = 1f;
	public int levelindex;

	#endregion

	#region Transition

	public void LoadMenu()
	{
		StartCoroutine(LoadLevel(levelindex));
	}

	IEnumerator LoadLevel(int levelindex)
	{
		transition.SetTrigger("Start");

		yield return new WaitForSeconds(transitiontime);

		SceneManager.LoadScene(levelindex);
	}
	#endregion
}
