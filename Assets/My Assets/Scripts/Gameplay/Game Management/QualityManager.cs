﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;
using UnityEngine.Audio;

public class QualityManager : MonoBehaviour
{
    #region Initialization

    public static bool isHighQuality = true;
    public GameObject highQualityButton, lowQualitybutton;
    public static bool isMusicOn = true;
    public GameObject musicOnButton, musicOffButton;
    public AudioMixer masterMixer;
    public GameObject PostProcessing;

    void Start()
    {
        CheckQuality();
    }

    #endregion

    #region Graphics Control

    public void SetHighQuality()
    {
        isHighQuality = true;
        CheckQuality();
    }
    public void SetLowQuality()
    {
        isHighQuality = false;
        CheckQuality();
    }
    #endregion

    #region Sound Setting
   
    public void MusicOn()
    {
        isMusicOn = true;
        masterMixer.SetFloat("MasterVolume", 0);
        CheckQuality();
    }

    public void MusicOff()
    {
        isMusicOn = false;
        masterMixer.SetFloat("MasterVolume", -80);
        CheckQuality();
    }
	#endregion

	#region Function

    public void CheckQuality()
	{
        if (isHighQuality)
        {
            highQualityButton.SetActive(true);
            lowQualitybutton.SetActive(false);
            PostProcessing.SetActive(true);

        }
        else if (!isHighQuality)
        {
            highQualityButton.SetActive(false);
            lowQualitybutton.SetActive(true);
            PostProcessing.SetActive(false);
        }

        if (isMusicOn)
        {
            musicOnButton.SetActive(true);
            musicOffButton.SetActive(false);
            masterMixer.SetFloat("MusicVolume", 0);
        }
        else if (!isMusicOn)
        {
            musicOnButton.SetActive(false);
            musicOffButton.SetActive(true);
            masterMixer.SetFloat("MusicVolume", -80);
        }
    }

	#endregion
}
