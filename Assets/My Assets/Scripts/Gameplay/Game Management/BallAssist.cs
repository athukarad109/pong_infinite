﻿/*
* Copyright (c) ElementalDestructor
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallAssist : MonoBehaviour
{
	#region Initialization
	public SavingData savingData;
	public ScoreController scoreController;
	public CamEnemySpawner enemySpawner;
	public CamTransition camTransition;
	public EnemyFinder enemyFinder;
	
	public float speed = 1.0f;
    public float multiplier;
    public float difficultyModifier;

	#endregion

	#region Function
	public void ScoredToLoad()
	{
		scoreController.Scored();

		enemySpawner.SpawnOpponent();

		enemyFinder.FindClosestEnemy();

		camTransition.MoveCam();

		DifficultyIncrease();

		savingData.FetchData();
	}

	public void DifficultyIncrease()
	{
		multiplier += difficultyModifier;
		speed = speed + multiplier;
	}

	#endregion
}
