﻿/*
* Copyright (c) ElementalDestructor
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavingData : MonoBehaviour
{
	#region Initialization

	public int Score;
	public int coffeecups;
	ScoreController scoreController;

	private void Start()
	{
		scoreController = gameObject.GetComponent<ScoreController>();
	}

	#endregion

	#region Saving Data


	public void FetchData()
	{
		Score = scoreController.PlayerScore;
	}

	public void SaveData()
	{
		SaveSystem.SaveData(this);
	}
	#endregion
}
