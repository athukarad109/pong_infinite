﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    #region Initialization

    public static bool GamePaused = true;
    AudioSource audioSource;

	private void Awake()
	{
        audioSource = GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>();
	}

	#endregion

	#region Game Pause

	public void Resume()
    {
        Time.timeScale = 1f;
        audioSource.UnPause();
        GamePaused = false;
    }

    public void Pause()
    {
        Time.timeScale = 0f;
        audioSource.Pause();
        GamePaused = true;
    }

    public void GameOverResume()
    {
        Time.timeScale = 1f;
    }

    #endregion
}
