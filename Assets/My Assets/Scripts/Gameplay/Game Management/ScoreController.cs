﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;
using TMPro;

public class ScoreController : MonoBehaviour
{
	#region Initialization

	public int PlayerScore = 0;
    private GameObject ScoreText;
    public Animator animator;

	private void Start()
	{
        ScoreText = GameObject.FindGameObjectWithTag("Score");
	}

	#endregion

	#region Funtion

	private void FixedUpdate()
    {
        TextMeshProUGUI uiPlayerScore = ScoreText.GetComponent<TextMeshProUGUI>();
        uiPlayerScore.text = PlayerScore.ToString();
    }

    public void Scored()
    {
        PlayerScore++;
        animator.SetTrigger("Scored");
    }

	#endregion
}
