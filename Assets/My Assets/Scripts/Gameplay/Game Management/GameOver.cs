﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class GameOver : MonoBehaviour
{
	#region Initialization

	public GameObject GameOverPanel;
	GameObject BallPrefab;
	private Movement movement;
	new Camera camera;
	public Animator animator;
	public PauseMenu pauseMenu;
	public SceneChanger sceneChanger;


	private void Start()
	{
		BallPrefab = GameObject.FindGameObjectWithTag("Ball");
		movement = BallPrefab.GetComponent<Movement>();
		sceneChanger = gameObject.GetComponent<SceneChanger>();
		pauseMenu = gameObject.GetComponent<PauseMenu>();
	}

	#endregion

	#region GameOver

	public void PlayGameOver()
	{
		if(PlayerPrefs.GetInt("Lives", 0) <= 0)
		{
			sceneChanger.LoadMenu();
		}
		else
		{
			GameOverPanel.SetActive(true);
			animator.SetTrigger("Start");
			PlayerController.isPlayerActive = false;
			PlayerController.startgame = false;
			PlayerController.gameStarted = false;
		}
	}

	public void Continue()
	{

		//deactivating game over screen
		GameOverPanel.SetActive(false);

		//activating the ball
		BallPrefab.SetActive(true);
		StartCoroutine(movement.StartBall());

		//getting the camera pos and setting ball to camera pos
		camera = Camera.main.GetComponent<Camera>();
		Vector2 cameraPos = camera.transform.position;
		BallPrefab.transform.position = cameraPos;

		PlayerController.isPlayerActive = true;
	}

	#endregion
}
