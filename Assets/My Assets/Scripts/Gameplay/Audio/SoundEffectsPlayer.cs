﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class SoundEffectsPlayer : MonoBehaviour
{
    public AudioSource ballhit;
    public AudioSource pointGained;

    public void ballHit()
    {
        ballhit.Play();
    }
    public void pointGainedSound()
    {
        pointGained.Play();
    }
}
