﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamShake : MonoBehaviour
{
	#region Camera Shake

	public IEnumerator Shake(float duration, float magnitude)
	{
		Vector3 originalPos = Camera.main.transform.localPosition;

		float elapsed = 0.0f;

		while (elapsed < duration)
		{
			float x = Random.Range(-1f, 1f) * magnitude;
			float y = Random.Range(-1f, 1f) * magnitude;

			Camera.main.transform.localPosition = new Vector3(originalPos.x + x, originalPos.y + y, originalPos.z);

			elapsed += Time.deltaTime;

			yield return null;
		}

		Vector3 OldPos = new Vector3(0f, originalPos.y, originalPos.z);
		Camera.main.transform.localPosition = OldPos;
	}

	#endregion
}
