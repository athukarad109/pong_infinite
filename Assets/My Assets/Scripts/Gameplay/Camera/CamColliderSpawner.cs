﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamColliderSpawner : MonoBehaviour
{
	#region Initialization

	public float colliderDepth = 4f;
	public float zPosition = 0f;
	Vector2 screenSize;
	Transform bottomCollider;
	Transform rightCollider;
	Transform leftCollider;
	Vector3 targetCamPos;

	#endregion

	#region Collider Generator
	private void Start()
	{
		//Generate our empty objects
		bottomCollider = new GameObject().transform;
		rightCollider = new GameObject().transform;
		leftCollider = new GameObject().transform;

		//Name our objects
		bottomCollider.name = "Bottom Collider";
		bottomCollider.tag = "Bottom Wall";
		rightCollider.name = "Right Collider";
		rightCollider.tag = "Right Wall";
		leftCollider.name = "Left Collider";
		leftCollider.tag = "Left Wall";

		//Add the collider
		bottomCollider.gameObject.AddComponent<BoxCollider2D>();
		bottomCollider.gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
		rightCollider.gameObject.AddComponent<BoxCollider2D>();
		leftCollider.gameObject.AddComponent<BoxCollider2D>();

		//Make them the child of Camera
		bottomCollider.parent = transform;
		rightCollider.parent = transform;
		leftCollider.parent = transform;

		//Generate world space point information for position and scale calculations
		//cameraPos = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z);
		targetCamPos = Camera.main.transform.position;
		screenSize.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
		screenSize.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;

		//Change scale and position to match the edges of the screen
		rightCollider.localScale = new Vector3(colliderDepth, screenSize.y * 2, colliderDepth);
		rightCollider.position = new Vector3(targetCamPos.x + screenSize.x + (rightCollider.localScale.x * 0.5f), targetCamPos.y, zPosition);
		leftCollider.localScale = new Vector3(colliderDepth, screenSize.y * 2, colliderDepth);
		leftCollider.position = new Vector3(targetCamPos.x - screenSize.x - (leftCollider.localScale.x * 0.5f), targetCamPos.y, zPosition);
		bottomCollider.localScale = new Vector3(screenSize.x * 2, colliderDepth, colliderDepth);
		bottomCollider.position = new Vector3(targetCamPos.x, targetCamPos.y - screenSize.y - (bottomCollider.localScale.y * 0.5f), zPosition);
	}
	#endregion
}
