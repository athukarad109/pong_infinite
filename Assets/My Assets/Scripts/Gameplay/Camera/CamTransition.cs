﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamTransition : MonoBehaviour
{
	#region Initialization

	public float speed = 5f;
	public float offset = 10f;
	Vector3 camPos;
	Vector3 targetballpos;
	public bool donttransition = false;
	public bool isTransitioning = false;
	public bool followball = false;
	public GameObject ball;
	private CamEnemySpawner enemySpawner;
	Vector3 targetCamPos;

	private void Awake()
	{
		ball = GameObject.FindGameObjectWithTag("Ball");
		enemySpawner = gameObject.GetComponent<CamEnemySpawner>();
		camPos = transform.position;
	}

	#endregion

	#region Camera Transition

	private void FixedUpdate()
	{
		if (isTransitioning)
		{
			float distance = Vector2.Distance(camPos, targetCamPos);
			float multiplier = distance / speed;
			camPos = Vector3.MoveTowards(camPos, targetCamPos, speed * Time.deltaTime);
			camPos = Vector3.Lerp(camPos, targetCamPos, Time.time * multiplier / 100);
			transform.position = camPos;
			if (transform.position == targetCamPos)
			{
				//Debug.Log("Target Pos Reached!");
				isTransitioning = false;
				donttransition = false;
			}
		}
		if (followball)
		{
			if (ball.transform.position.y > targetballpos.y)
			{
				//Debug.Log("Ball Reached");
				followball = false;
				donttransition = true;
				isTransitioning = true;
				Movement.canBounce = true;
			}
			else
			{
				Vector3 ballpos = new Vector3(camPos.x, ball.transform.position.y - 1f, camPos.z);
				float distance = Vector2.Distance(camPos, ballpos);
				float multiplier = distance / speed;
				camPos = Vector3.MoveTowards(camPos, ballpos, speed * Time.deltaTime);
				camPos = Vector3.Lerp(camPos, ballpos, Time.time * multiplier / 100);
				transform.position = camPos;
			}
		}

	}

	public void MoveCam()
	{
		if (!donttransition)
		{
			followball = true;
			Movement.canBounce = false;

			Vector3 enemypos = enemySpawner.newOp.gameObject.GetComponent<BoxCollider2D>().transform.position;

			targetCamPos = new Vector3(camPos.x, enemypos.y, camPos.z);
			targetballpos = new Vector3(0, targetCamPos.y, 0);
		}
	}

	#endregion
}
