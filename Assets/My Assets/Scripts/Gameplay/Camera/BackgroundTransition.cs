﻿using UnityEngine;

public class BackgroundTransition : MonoBehaviour
{
    private Transform background_1;
    private Transform background_2;
    private float size;

    private void Start()
    {
        background_1 = GameObject.FindGameObjectWithTag("Bg1").transform;
        background_2 = GameObject.FindGameObjectWithTag("Bg2").transform;
        size = background_1.GetComponent<BoxCollider2D>().size.y;
    }

    private void FixedUpdate()
    {
        if (transform.position.y >= background_2.position.y)
        {
            background_1.position = new Vector3(background_1.position.x, background_2.position.y + size, background_1.position.z);
            SwitchBackground();
        }
    }

    private void SwitchBackground()
    {
        Transform bg = background_1;
        background_1 = background_2;
        background_2 = bg;
    }
}
