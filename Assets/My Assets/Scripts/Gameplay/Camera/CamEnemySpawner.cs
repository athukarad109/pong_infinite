﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamEnemySpawner : MonoBehaviour
{
	#region Initialization

	public GameObject Opponent;
	public GameObject newOp;
	public float OppOffset;
	Vector2 opNewPos;
	public GameObject opponentCollection;

	#endregion

	#region Opponent Spawner

	public void SpawnOpponent()
	{
		Vector2 opSpawnPosition = new Vector2(opNewPos.x, opNewPos.y + OppOffset);

		newOp = Instantiate(Opponent, opSpawnPosition, transform.rotation);

		opNewPos = newOp.transform.position;

		newOp.transform.parent = opponentCollection.transform;
	}
	#endregion
}
