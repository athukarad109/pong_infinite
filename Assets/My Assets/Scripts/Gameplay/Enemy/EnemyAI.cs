﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    /* #region Initialization

     public float multiplier;
     public float sign;
     public float difficultyModifier;
     public float xOffset;

     #endregion

     #region Function

     private void Start()
     {
         int i = Random.Range(0, 10);
         if(i <= 5)
             sign = 1;
         if (i >= 5)
             sign = -1;
     }

     private void Update()
     {
         transform.Translate(Vector2.right * sign * multiplier * Time.deltaTime);
         SwtichSide();
     }

     public void DifficultyIncrease()
     {
         multiplier += difficultyModifier;
     }

     public void SwtichSide()
     {
         if(transform.position.x - xOffset > Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x)
         {
             sign = -1;
         }
         else if(transform.position.x + xOffset < Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x)
         {
             sign = 1;
         }
     }

     #endregion*/

    [SerializeField] Transform ball;
    private BallAssist BallAssist;
    public float xOffset = 1.5f;


    private void Start()
	{
        ball = GameObject.FindGameObjectWithTag("Ball").transform;
        BallAssist = GameObject.FindGameObjectWithTag("Manager").GetComponent<BallAssist>();
	}

    void Update()
    {
        Confine();
        Vector3 ballMove = new Vector3(ball.position.x * BallAssist.speed, 0f, 0f) * Time.deltaTime;
        transform.Translate(ballMove);

    }

    void Confine()
	{
        if(transform.position.x >= xOffset)
		{
            transform.position = new Vector2(xOffset, transform.position.y);
		}
        if (transform.position.x <= -xOffset)
		{
            transform.position = new Vector2(-xOffset, transform.position.y);

        }
    }
}