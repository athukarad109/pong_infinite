﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMainMenu : MonoBehaviour
{
    public void GoToMainMenu()
	{
		SceneManager.LoadScene("Main Menu");
	}
}
