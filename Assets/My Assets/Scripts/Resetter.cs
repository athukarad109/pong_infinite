﻿/*
* Copyright (c) ElementalDestructor
*/
using UnityEngine;

public class Resetter : MonoBehaviour
{
    static bool Reseted = false;
    
    void Start()
    {
		if (!Reseted)
		{
            PlayerPrefs.SetInt("HighScore", 0);
            PlayerPrefs.SetInt("ScoreUpload", 0);
            PlayerPrefs.SetInt("Coffee", 0);
        }
    }
}
